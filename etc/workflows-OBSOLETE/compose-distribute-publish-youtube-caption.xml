<?xml version="1.0" encoding="UTF-8"?>
<definition xmlns="http://workflow.opencastproject.org">

  <id>full-youtube-caption</id>
  <title>Encode, Analyze, and Distribute Youtube with captioning</title>
  <tags>
    <tag>upload</tag>
    <tag>schedule</tag>
    <tag>archive</tag>
  </tags>
  <description>
    A simple workflow that transcodes the media into distribution formats, then sends the resulting distribution files,
    along with their associated metadata, to the distribution channels.
  </description>

  <configuration_panel>
  <![CDATA[
    <fieldset>
      <legend>Holds</legend>
      <span>Processing should be paused to allow for:</span>
      <ul class="oc-ui-checkbox-list" id="hold-ul">
        <li class="ui-helper-clearfix">
          <span id="trimholdconfig">
            <input id="trimHold" name="trimHold" type="checkbox" class="configField holdCheckbox" value="true" />
            <span id="i18n_hold_for_trim">&nbsp;Review / Trim before encoding (with option to edit info)</span>
          </span>
        </li>
        <li class="ui-helper-clearfix" style="display:none;">
          <span id="captionconfig">
            <input id="captionHold" name="captionHold" type="checkbox" checked="checked" disabled="disabled" class="configField holdCheckbox" value="true"/>
            <span id="i18n_hold_for_caption">&nbsp;Captions file upload</span>
          </span>
        </li>
      </ul>
    </fieldset>
    <fieldset>
      <legend>Distribution</legend>
      <ul class="oc-ui-form-list">
        <li class="ui-helper-clearfix">
          <label class="scheduler-label"><span class="color-red">* </span><span id="i18n_dist_label">Distribution Channel(s)</span>:</label>
          <span id="dist">
            <input id="distribution" name="distribution" type="checkbox" checked="checked" disabled="disabled" class="configField" value="Matterhorn Media Module" />
            <span id="i18n_dist_mmm">&nbsp;Matterhorn Media Module</span>
            <input id="youtube" name="youtube" type="checkbox" class="configField" checked="checked" disabled="disabled" value="true" />
            <span id="i18n_dist_mmm">&nbsp;Youtube</span>
            <input id="itunesu" name="itunesu" type="checkbox" checked="checked" disabled="disabled" class="configField" value="iTunesU" />
            <span id="i18n_dist_mmm">&nbsp;iTunesU</span>
          </span>
        </li>
      </ul>
    </fieldset>
    <script type="text/javascript">
      var ocWorkflowPanel = ocWorkflowPanel || {};
      ocWorkflowPanel.registerComponents = function(components){
        /* components with keys that begin with 'org.opencastproject.workflow.config' will be passed
         * into the workflow. The component's nodeKey must match the components array key.
         *
         * Example:'org.opencastproject.workflow.config.myProperty' will be availible at ${my.property}
         */
        components['org.opencastproject.workflow.config.trimHold'] = new ocAdmin.Component(
          ['trimHold'],
          {key: 'org.opencastproject.workflow.config.trimHold'});
          
        components['org.opencastproject.workflow.config.captionHold'] = new ocAdmin.Component(
          ['captionHold'],
          {key: 'org.opencastproject.workflow.config.captionHold'});

        components['org.opencastproject.workflow.config.youtube'] = new ocAdmin.Component(
          ['youtube'],
          {key: 'org.opencastproject.workflow.config.youtube'});

        components['org.opencastproject.workflow.config.itunesu'] = new ocAdmin.Component(
          ['itunesu'],
          {key: 'org.opencastproject.workflow.config.itunesu'});
          
          //etc...
      }

      ocWorkflowPanel.setComponentValues = function(values, components){
        components['org.opencastproject.workflow.config.captionHold'].setValue(values['org.opencastproject.workflow.config.captionHold']);
        components['org.opencastproject.workflow.config.trimHold'].setValue(values['org.opencastproject.workflow.config.trimHold']);
        components['org.opencastproject.workflow.config.youtube'].setValue(values['org.opencastproject.workflow.config.youtube']);
      }
    </script>
  ]]>
  </configuration_panel>

  <operations>
    
    <!-- tag the incoming source material for archival -->
    <operation
      id="tag"
      description="Tagging source material for archival">
      <configurations>
        <configuration key="source-flavors">*/source</configuration>
        <configuration key="target-tags">+archive</configuration>
      </configurations>
    </operation>

    <!-- tag dublincore/* catalogs with +engage so they will be distributed and added to search -->
    <operation
      id="tag"
      description="Tagging dublin core catalogs for publishing">
      <configurations>
        <configuration key="source-flavors">dublincore/*</configuration>
        <configuration key="target-tags">+engage</configuration>
      </configurations>
    </operation> 

    <!-- inspect the media -->
    <operation
      id="inspect"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Inspecting the media package">
    </operation>

    <!-- prepare audio/video tracks -->
    <!-- Begin Epiphan MCA operations -->

    <!-- This section is only relevant for the Epiphan MCA devices, however
         it needs to be present for those to work.  Since it does not affect
         normal CA ingests we will leave this in -->

	<!-- compose multitrack/source to presenter/source using epiphan.presenter -->
    <operation
      id="compose"
      fail-on-error="false"
      exception-handler-workflow="error"
      description="Splitting Media (if needed)">
      <configurations>
        <configuration key="source-flavor">multitrack/source</configuration>
        <configuration key="target-flavor">presenter/source</configuration>
        <configuration key="encoding-profile">epiphan.presenter</configuration>
      </configurations>
    </operation>
	<!-- compose multitrack/source to presentation/source using epiphan.presentation -->
	<operation
      id="compose"
      fail-on-error="false"
      exception-handler-workflow="error"
      description="Splitting Media (if needed)">
      <configurations>
        <configuration key="source-flavor">multitrack/source</configuration>
        <configuration key="target-flavor">presentation/source</configuration>
        <configuration key="encoding-profile">epiphan.presentation</configuration>
      </configurations>
    </operation>

    <!-- End Epiphan MCA operations -->

	<!-- prepare-av presenter/source -> presenter/work -->
    <operation
      id="prepare-av"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Preparing presenter audio and video work versions">
      <configurations>
        <configuration key="source-flavor">presenter/source</configuration>
        <configuration key="target-flavor">presenter/work</configuration>
        <configuration key="rewrite">false</configuration>
        <configuration key="promiscuous-audio-muxing">true</configuration>
      </configurations>
    </operation>
	<!-- prepare-av presentation/source -> presentation/work -->
	<operation
      id="prepare-av"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Preparing presentation audio and video work version">
      <configurations>
        <configuration key="source-flavor">presentation/source</configuration>
        <configuration key="target-flavor">presentation/work</configuration>
        <configuration key="rewrite">false</configuration>
        <configuration key="promiscuous-audio-muxing">true</configuration>
      </configurations>
    </operation>

    <!-- encode to hold preview player formats -->

	<!-- if (trimHold), compose presenter/work to presenter/preview using flash-preview.http -->
    <operation
      id="compose"
      if="${trimHold}"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Encoding presenter (camera) video for preview">
      <configurations>
        <configuration key="source-flavor">presenter/work</configuration>
        <configuration key="target-flavor">presenter/preview</configuration>
        <configuration key="encoding-profile">flash-preview.http</configuration>
      </configurations>
    </operation>
    <!-- if (trimHold), compose presentation/work to presentation/preview using flash-preview.http -->
	<operation
      id="compose"
      if="${trimHold}"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Encoding presentation (screen) for preview">
      <configurations>
        <configuration key="source-flavor">presentation/work</configuration>
        <configuration key="target-flavor">presentation/preview</configuration>
        <configuration key="encoding-profile">flash-preview.http</configuration>
      </configurations>
    </operation>
    <!-- if (trimHold), trim */work to /trimmed using trim.work -->
    <operation
      id="trim"
      if="${trimHold}"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Waiting for user to review / trim recording">
      <configurations>
        <configuration key="source-flavor">*/work</configuration>
        <configuration key="target-flavor-subtype">trimmed</configuration>
        <configuration key="encoding-profile">trim.work</configuration>
      </configurations>
    </operation>  

    <!-- image presenter/trimmed for engage using player-preview.http (engage player preview images)-->
    <operation
      id="image"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Encoding presenter (camera) to player preview image">
      <configurations>
        <configuration key="source-flavor">presenter/trimmed</configuration>
        <configuration key="source-tags"></configuration>
        <configuration key="target-flavor">presenter/player+preview</configuration>
        <configuration key="target-tags">engage</configuration>
        <configuration key="encoding-profile">player-preview.http</configuration>
        <configuration key="time">1</configuration>
      </configurations>
    </operation>
    <!-- image presentation/trimmed for engage using player-preview.http (engage player preview images)-->
	<operation
      id="image"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Encoding presentation (screen) to player preview image">
      <configurations>
        <configuration key="source-flavor">presentation/trimmed</configuration>
        <configuration key="source-tags"></configuration>
        <configuration key="target-flavor">presentation/player+preview</configuration>
        <configuration key="target-tags">engage</configuration>
        <configuration key="encoding-profile">player-preview.http</configuration>
        <configuration key="time">1</configuration>
      </configurations>
    </operation>

    <!-- compose presenter/trimmed for engage using flash.http -->
	<operation
      id="compose"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Encoding presenter (camera) video to Flash download">
      <configurations>
        <configuration key="source-flavor">presenter/trimmed</configuration>
        <configuration key="target-flavor">presenter/delivery</configuration>
        <configuration key="target-tags">engage</configuration>
        <configuration key="encoding-profile">flash.http</configuration>
      </configurations>
    </operation>
    <!-- compose presentation/trimmed for engage using flash.http -->
	<operation
      id="compose"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Encoding presentation (screen) to Flash download">
      <configurations>
        <configuration key="source-flavor">presentation/trimmed</configuration>
        <configuration key="target-flavor">presentation/delivery</configuration>
        <configuration key="target-tags">engage</configuration>
        <configuration key="encoding-profile">flash-vga.http</configuration>
      </configurations>
    </operation>
    <!-- compose presenter/trimmed for engage using flash-audio.http -->
	<operation
      id="compose"
      fail-on-error="false"
      exception-handler-workflow="error"
      description="Encoding presenter (screen) to flash audio download">
      <configurations>
        <configuration key="source-flavor">presenter/trimmed</configuration>
        <configuration key="target-flavor">presenter/delivery</configuration>
        <configuration key="target-tags">engage</configuration>
        <configuration key="encoding-profile">flash-audio.http</configuration>
      </configurations>
    </operation>   
     
	<!-- segment-video for presentation/trimmed -->
    <operation
      id="segment-video"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Extracting segments from presentation">
      <configurations>
        <configuration key="source-flavor">presentation/trimmed</configuration>
      </configurations>
    </operation>
	<!-- image presenter/trimmed for search using search-cover.http (search results preview images)-->
    <operation
      id="image"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Encoding presenter (camera) to search result preview image">
      <configurations>
        <configuration key="source-flavor">presenter/trimmed</configuration>
        <configuration key="source-tags"></configuration>
        <configuration key="target-flavor">presenter/search+preview</configuration>
        <configuration key="target-tags">engage</configuration>
        <configuration key="encoding-profile">search-cover.http</configuration>
        <configuration key="time">1</configuration>
      </configurations>
    </operation>
	<!-- image presentation/trimmed for search using search-cover.http (search results preview images)-->
    <operation
      id="image"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Encoding presentation (screen) to search result preview image">
      <configurations>
        <configuration key="source-flavor">presentation/trimmed</configuration>
        <configuration key="source-tags"></configuration>
        <configuration key="target-flavor">presentation/search+preview</configuration>
        <configuration key="target-tags">engage</configuration>
        <configuration key="encoding-profile">search-cover.http</configuration>
        <configuration key="time">1</configuration>
      </configurations>
    </operation>

    <!--  encode to feed distribution formats -->
	<!-- compose presenter/trimmed for audio rss feeds-->
    <operation
      id="compose"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Encoding presenter (camera) to mpeg4 audio download">
      <configurations>
        <configuration key="source-flavor">presenter/trimmed</configuration>
        <configuration key="target-flavor">presenter/delivery</configuration>
        <configuration key="target-tags">rss, atom</configuration>
        <configuration key="encoding-profile">feed-m4a.http</configuration>
      </configurations>
    </operation>
	<!-- compose presentation/trimmed for audio rss feeds-->
    <operation
      id="compose"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Encoding presentation (screen) to mpeg4 audio download">
      <configurations>
        <configuration key="source-flavor">presentation/trimmed</configuration>
        <configuration key="target-flavor">presentation/delivery</configuration>
        <configuration key="target-tags">rss, atom</configuration>
        <configuration key="encoding-profile">feed-m4a.http</configuration>
      </configurations>
    </operation>
	<!-- if (youtube), compose presentation/trimmed for youtube using youtube-hd.http -->
    <operation
      if="${youtube}"
      id="compose"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Encoding presentation (screen) to youtube">
      <configurations>
        <configuration key="source-flavor">presentation/trimmed</configuration>
        <configuration key="target-flavor">presentation/delivery</configuration>
        <configuration key="target-tags">youtubein</configuration>
        <configuration key="encoding-profile">youtube-hd.http</configuration>
      </configurations>
    </operation>
    <!-- if (youtube), compose presenter/trimmed for youtube using youtube-hd.http -->
	<operation
      if="${youtube}"
      id="compose"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Encoding presenter to youtube">
      <configurations>
        <configuration key="source-flavor">presenter/trimmed</configuration>
        <configuration key="target-flavor">presenter/delivery</configuration>
        <configuration key="target-tags">youtubein</configuration>
        <configuration key="encoding-profile">youtube-hd.http</configuration>
      </configurations>
    </operation>
    <!-- image presenter/trimmed for rss feed preview images -->
    <operation
      id="image"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Encoding presenter (camera) to feed preview image">
      <configurations>
        <configuration key="source-flavor">presenter/trimmed</configuration>
        <configuration key="source-tags"></configuration>
        <configuration key="target-flavor">presenter/feed+preview</configuration>
        <configuration key="target-tags">rss, atom</configuration>
        <configuration key="encoding-profile">feed-cover.http</configuration>
        <configuration key="time">1</configuration>
      </configurations>
    </operation>
    <!-- image presentation/trimmed for rss feed preview images -->
	<operation
      id="image"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Encoding presentation (screen) to feed preview image">
      <configurations>
        <configuration key="source-flavor">presentation/trimmed</configuration>
        <configuration key="source-tags"></configuration>
        <configuration key="target-flavor">presentation/feed+preview</configuration>
        <configuration key="target-tags">rss, atom</configuration>
        <configuration key="encoding-profile">feed-cover.http</configuration>
        <configuration key="time">1</configuration>
      </configurations>
    </operation>

    <!-- segmentpreviews presentation/trimmed for engage using player-slides.http -->  
    <operation
      id="segmentpreviews"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Encoding presentation (screen) to segment preview image">
      <configurations>
        <configuration key="source-flavor">presentation/trimmed</configuration>
        <configuration key="source-tags"></configuration>
        <configuration key="target-flavor">presentation/segment+preview</configuration>
        <configuration key="reference-flavor">presentation/delivery</configuration>
        <configuration key="reference-tags">engage</configuration>
        <configuration key="target-tags">engage</configuration>
        <configuration key="encoding-profile">player-slides.http</configuration>
      </configurations>
    </operation>

    <!-- extract-text on presentation/trimmed for engage -->
    <operation
      id="extract-text"
      fail-on-error="false"
      exception-handler-workflow="error"
      description="Extracting text from presentation segments">
      <configurations>
        <configuration key="source-flavor">presentation/trimmed</configuration>
        <configuration key="source-tags"></configuration>
        <configuration key="target-tags">engage</configuration>
      </configurations>
    </operation>

    <!-- coverart - Add the cover art attachment -->
    <operation
      id="coverart"
      fail-on-error="false"
      description="Adding cover art">
      <configurations>
        <configuration key="target-tags">distributed</configuration>
      </configurations>
    </operation>
    
    <!-- apply-acl - from series to the mediapackage -->
    <operation
      id="apply-acl"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Applying access control rules">
    </operation>

    <!-- archive - the current state of the mediapackage -->
    <operation
      id="archive"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Archived">
    </operation>
    
	<!-- distribute-download for engage -->
    <operation
      id="distribute-download"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Distributing to progressive downloads">
      <configurations>
        <configuration key="source-tags">engage,atom,rss</configuration>
        <configuration key="target-tags">distributed</configuration>
      </configurations>
    </operation>

	<!-- if (org.opencastproject.streaming.url), distribute-streaming for engage -distributed -->
    <operation
      id="distribute-streaming"
      fail-on-error="true"
      if="${org.opencastproject.streaming.url}"
      exception-handler-workflow="error"
      description="Distributing to streaming server">
      <configurations>
        <configuration key="source-tags">engage,-distributed</configuration>
        <configuration key="target-tags">distributed</configuration>
      </configurations>
    </operation>
 
    <!-- archive - the current state of the mediapackage -->
    <operation
      id="archive"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Archiving">
    </operation>
 
    <!-- Moving this to after final archive because archiving YouTube dist is broken -->
    <!-- if (youtube), distribute-youtube  -->
	<operation
      if="${youtube}"
      id="distribute-youtube"
      fail-on-error="false"
      exception-handler-workflow="error"
      description="Distributing media to YouTube">
      <configurations>
        <configuration key="source-tags">youtubein</configuration>
        <configuration key="target-tags">youtube</configuration>
      </configurations>
    </operation>
    
    <!-- send-email -to webcast@berkeley.edu notifying that a file needing capture is on youTube -->
	<operation
	    id="send-email"
	    fail-on-error="true"
	    exception-handler-workflow="error"
	    description="Sends email">
	    <configurations>
	        <configuration key="to">webcast@berkeley.edu</configuration>
	        <configuration key="subject">Captionable file uploaded to youTube</configuration>
	    </configurations>
	</operation>
	
	<!-- caption -->
    <operation
      id="caption"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Waiting for user to upload captions">
      <configurations>
        <configuration key="caption-flavor">caption/dfxp</configuration>
      </configurations>
    </operation>
    
    <!-- publish to engage search index -->
    <operation
      id="publish"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Published">
      <configurations>
        <configuration key="source-tags">distributed</configuration>
      </configurations>
    </operation>
 
    <!-- cleanup preserving */source,dublincore/* -->
    <operation
      id="cleanup"
      fail-on-error="false"
      description="Cleaning up">
      <configurations>
        <configuration key="preserve-flavors">*/source,dublincore/*,mpeg-7/*</configuration>
      </configurations>
    </operation>
  </operations>

</definition>
