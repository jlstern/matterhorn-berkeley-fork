<?xml version="1.0" encoding="UTF-8"?>
<definition xmlns="http://workflow.opencastproject.org">
  <title>Distribution (UCB standard)</title>
  <id>publish-ucb-standard</id>
  <tags>
      <tag>upload</tag>
      <tag>schedule</tag>
      <tag>archive</tag>
  </tags>
  <description>Distribution (UCB standard) distributes according to standard UCB Classroom Capture specs. The RSS feed can be used for iTunes U distribution.</description>

  <configuration_panel><![CDATA[
    <fieldset>
      <legend>Interruptions</legend>     
      <ul class="oc-ui-form-list">
        <li class="ui-helper-clearfix">
          <label class="scheduler-label"><span class="color-red">* </span>Delay publishing until: </label>
          <span id="publishdelayconfig">
            <input id="publishDelayHold" name="publishDelayHold" type="checkbox" class="configField holdCheckbox" value="true" />
            <input type="text" size="10" id="publishDate" name="available" class="dc-metadata-field" />
          </span>
        </li>
      </ul> 
    </fieldset>

    <fieldset>
      <legend>Distribution</legend>
      <ul class="oc-ui-form-list">
        <li class="ui-helper-clearfix">
          <label class="scheduler-label"><span class="color-red">* </span><span id="i18n_dist_label">Distribution Channel(s)</span>:</label>
            <span id="dist">
              <input id="mmm" name="mmm" type="checkbox" class="configField default" value="true" />
              <span id="i18n_dist_mmm">&nbsp;Matterhorn Media Module</span>

              <input id="youtube" name="youtube" type="checkbox" class="configField default" value="true" />
              <span id="i18n_dist_youtube">&nbsp;YouTube</span>
            </span>
          </span>
        </li>
      </ul>
    </fieldset>

    <label class="scheduler-label"></label><a href='' id="resetWorkflowDefaults">Reset to defaults for these instructions</a>

    <script type="text/javascript">

      var ocWorkflowPanel = ocWorkflowPanel || {};
      
       var initializerDate;
       initializerDate = new Date();
       $('#publishDate').datepicker({
        showOn: 'both',
        buttonImage: '/admin/img/icons/calendar.gif',
        buttonImageOnly: true,
        dateFormat: 'yy-mm-dd'
      });
      $('#publishDate').datepicker('setDate', initializerDate);

      $("#resetWorkflowDefaults").click( function() {
        $(".configField").removeAttr('checked');
        $(".configField.default").attr('checked','checked');
        return false;
      });

      var eventId = ocUtils.getURLParam('eventId');
      if(!eventId || !ocUtils.getURLParam('edit')) {
        $("#resetWorkflowDefaults").click();
      }

      ocWorkflowPanel.registerComponents = function(components){
        /* components with keys that begin with 'org.opencastproject.workflow.config' will be passed
         * into the workflow. The component's nodeKey must match the components array key.
         *
         * Example:'org.opencastproject.workflow.config.myProperty' will be availible at ${my.property}
         */
        components['org.opencastproject.workflow.config.youtube'] = new ocAdmin.Component(
          ['youtube'],
          {key: 'org.opencastproject.workflow.config.youtube'},
          {getValue: function(){ return this.fields.youtube.is(":checked");}}
        );
        components['org.opencastproject.workflow.config.mmm'] = new ocAdmin.Component(
          ['mmm'],
          {key: 'org.opencastproject.workflow.config.mmm'},
          {getValue: function(){ return this.fields.mmm.is(":checked");}}
        );
      }

      ocWorkflowPanel.setComponentValues = function(values, components){
        components['org.opencastproject.workflow.config.youtube'].setValue(values['org.opencastproject.workflow.config.youtube']);
        components['org.opencastproject.workflow.config.mmm'].setValue(values['org.opencastproject.workflow.config.mmm']);
      }
    </script>
  ]]></configuration_panel>

  <operations>
  
    <operation id="publish-delayed" fail-on-error="true" description="Waiting for Publish Date">
    </operation>
    
    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
    <!-- Apply the video segmentation algorithm to the presentation tracks -->
    <!-- and extract segment preview images and metadata.                  -->
    <!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->    
    <operation
      id="segment-video"
      fail-on-error="false"
      exception-handler-workflow="error"
      description="Creating video segments out of presentation video">
      <configurations>
        <configuration key="source-flavor">presentation/trimmed</configuration>
      </configurations>
    </operation>

    <operation
        id="segmentpreviews"
        fail-on-error="false"
        exception-handler-workflow="error"
        description="Creating a preview image for each video segment">
      <configurations>
        <configuration key="source-flavor">presentation/trimmed</configuration>
        <configuration key="source-tags"></configuration>
        <configuration key="target-flavor">presentation/segment+preview</configuration>
        <configuration key="reference-flavor">presentation/delivery</configuration>
        <configuration key="reference-tags">engage</configuration>
        <configuration key="target-tags">engage</configuration>
        <configuration key="encoding-profile">player-slides-high-resolution.http</configuration>
      </configurations>
    </operation>

    <operation
        id="image"
        fail-on-error="true"
        exception-handler-workflow="error"
        description="Create cover image of presenter (for feed)">
      <configurations>
        <configuration key="source-flavor">presenter/trimmed</configuration>
        <configuration key="source-tags"></configuration>
        <configuration key="target-flavor">presenter/feed+preview</configuration>
        <configuration key="target-tags">rss, atom</configuration>
        <configuration key="encoding-profile">feed-cover.http</configuration>
        <configuration key="time">1</configuration>
      </configurations>
    </operation>

    <operation
        id="image"
        fail-on-error="true"
        exception-handler-workflow="error"
        description="Create cover image of presentation (for feed)">
      <configurations>
        <configuration key="source-flavor">presentation/trimmed</configuration>
        <configuration key="source-tags"></configuration>
        <configuration key="target-flavor">presentation/feed+preview</configuration>
        <configuration key="target-tags">rss, atom</configuration>
        <configuration key="encoding-profile">feed-cover.http</configuration>
        <configuration key="time">1</configuration>
      </configurations>
    </operation>

    <operation
      id="caption"
      if="${captionHold}"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Wait for user to upload captions">
      <configurations>
        <configuration key="target-tags">engage,archive</configuration>
      </configurations>
    </operation>

    <operation
        if="${youtube}"
        id="merge-videos"
        fail-on-error="true"
        exception-handler-workflow="error"
        description="Create a side-by-side composition of presenter and presentation videos">
      <configurations>
        <configuration key="source-flavors">presenter/trimmed,presentation/trimmed</configuration>
        <configuration key="target-flavor">presenter/youtube</configuration>
        <configuration key="encoding-profile">video-overlay.work</configuration>
        <configuration key="target-tag">youtubein</configuration>
      </configurations>
    </operation>

    <operation
        if="${youtube}"
        id="inspect"
        fail-on-error="true"
        exception-handler-workflow="error"
        description="Inspect video prior to YouTube publication">
    </operation>

    <!-- Next, Engage and RSS specific operations. -->
    <!--  (1) Encode to engage player preview images -->
    <!--  (2) Image presenter/trimmed for engage using player-preview.http (engage player preview images)-->

    <operation
        id="image"
        fail-on-error="true"
        exception-handler-workflow="error"
        description="Create preview image of presenter for Engage player">
      <configurations>
        <configuration key="source-flavor">presenter/trimmed</configuration>
        <configuration key="source-tags"></configuration>
        <configuration key="target-flavor">presenter/player+preview</configuration>
        <configuration key="target-tags">engage</configuration>
        <configuration key="encoding-profile">player-preview.http</configuration>
        <configuration key="time">1</configuration>
      </configurations>
    </operation>

    <operation
        id="image"
        fail-on-error="true"
        exception-handler-workflow="error"
        description="Create preview image of presentation for Engage player">
      <configurations>
        <configuration key="source-flavor">presentation/trimmed</configuration>
        <configuration key="source-tags"></configuration>
        <configuration key="target-flavor">presentation/player+preview</configuration>
        <configuration key="target-tags">engage</configuration>
        <configuration key="encoding-profile">player-preview.http</configuration>
        <configuration key="time">1</configuration>
      </configurations>
    </operation>

    <operation
        id="compose"
        fail-on-error="true"
        exception-handler-workflow="error"
        description="Create Flash video of presenter for download">
      <configurations>
        <configuration key="source-flavor">presenter/trimmed</configuration>
        <configuration key="target-flavor">presenter/delivery</configuration>
        <configuration key="target-tags">engage</configuration>
        <configuration key="encoding-profile">hd.stream</configuration>
      </configurations>
    </operation>

    <operation
        id="compose"
        fail-on-error="true"
        exception-handler-workflow="error"
        description="Create Flash video of presentation for download">
      <configurations>
        <configuration key="source-flavor">presentation/trimmed</configuration>
        <configuration key="target-flavor">presentation/delivery</configuration>
        <configuration key="target-tags">engage</configuration>
        <configuration key="encoding-profile">hd.stream</configuration>
      </configurations>
    </operation>

    <operation
        id="compose"
        fail-on-error="false"
        exception-handler-workflow="error"
        description="Create Flash audio of presenter for download">
      <configurations>
        <configuration key="source-flavor">presenter/trimmed</configuration>
        <configuration key="target-flavor">presenter/delivery</configuration>
        <configuration key="target-tags">engage</configuration>
        <configuration key="encoding-profile">hd-audio.stream</configuration>
      </configurations>
    </operation>

    <operation
        id="image"
        fail-on-error="true"
        exception-handler-workflow="error"
        description="Create preview image of presenter (used in search results)">
      <configurations>
        <configuration key="source-flavor">presenter/trimmed</configuration>
        <configuration key="source-tags"></configuration>
        <configuration key="target-flavor">presenter/search+preview</configuration>
        <configuration key="target-tags">engage</configuration>
        <configuration key="encoding-profile">search-cover.http</configuration>
        <configuration key="time">1</configuration>
      </configurations>
    </operation>

    <operation
        id="image"
        fail-on-error="true"
        exception-handler-workflow="error"
        description="Create preview image of presentation (used in search results)">
      <configurations>
        <configuration key="source-flavor">presentation/trimmed</configuration>
        <configuration key="source-tags"></configuration>
        <configuration key="target-flavor">presentation/search+preview</configuration>
        <configuration key="target-tags">engage</configuration>
        <configuration key="encoding-profile">search-cover.http</configuration>
        <configuration key="time">1</configuration>
      </configurations>
    </operation>

    <operation
        id="compose"
        fail-on-error="true"
        exception-handler-workflow="error"
        description="Create media for RSS feed from presenter video">
      <configurations>
        <configuration key="source-flavor">presenter/trimmed</configuration>
        <configuration key="target-flavor">presenter/delivery</configuration>
        <configuration key="target-tags">rss, atom</configuration>
        <configuration key="encoding-profiles">feed-m4a.http, itunes-mpeg-4.http</configuration>
      </configurations>
    </operation>

    <operation
        id="compose"
        fail-on-error="true"
        exception-handler-workflow="error"
        description="Create media for RSS feed from presentation video">
      <configurations>
        <configuration key="source-flavor">presentation/trimmed</configuration>
        <configuration key="target-flavor">presentation/delivery</configuration>
        <configuration key="target-tags">rss, atom</configuration>
        <configuration key="encoding-profiles">feed-m4a.http, itunes-mpeg-4.http</configuration>
      </configurations>
    </operation>

    <operation
      id="publish-engage"
      max-attempts="2"
      fail-on-error="true"
      exception-handler-workflow="error"
      description="Publish to Engage player">
      <configurations>
        <configuration key="download-source-tags">engage,atom,rss</configuration>
        <!--<configuration key="streaming-source-tags">engage</configuration>-->
        <configuration key="check-availability">true</configuration>
      </configurations>
    </operation>
    
    <operation
        id="publish-youtube"
        if="${youtube}"
        max-attempts="2"
        fail-on-error="true"
        exception-handler-workflow="error"
        description="Publish to YouTube">
      <configurations>
        <configuration key="source-flavors">presenter/youtube</configuration>
        <configuration key="source-tags">youtubein</configuration>
        <configuration key="target-tags">+youtube,+archive</configuration>
      </configurations>
    </operation>
    
    <operation
        id="archive"
        fail-on-error="true"
        exception-handler-workflow="error"
        description="Archiving">
      <configurations>
        <configuration key="source-tags">archive</configuration>
      </configurations>
    </operation>

    <operation
      id="cleanup"
      fail-on-error="false"
      description="Clean up and finish">
      <configurations>
        <configuration key="preserve-flavors"></configuration>
        <configuration key="delete-external">true</configuration>
      </configurations>
    </operation>
  </operations>

</definition>