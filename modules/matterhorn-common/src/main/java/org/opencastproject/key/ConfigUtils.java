/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.key;

import org.osgi.framework.BundleContext;

import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

/**
 * @author John Crossman
 */
public final class ConfigUtils {

  private ConfigUtils() {
  }

  /**
   * Use {@link ConfigKey} paradigm to pull properties.
   * @param properties Null not allowed
   * @param configKeys One or more keys to use for property get.
   * @return Never null
   */
  public static List<String> get(final Properties properties, final ConfigKey... configKeys) {
    final List<String> list = new LinkedList<String>();
    for (final ConfigKey configKey : configKeys) {
      list.add(properties.getProperty(configKey.getKey()));
    }
    return list;
  }

  /**
   * Use {@link ConfigKey} paradigm to pull properties.
   * @param bundleContext Null not allowed
   * @param configKeys One or more keys to use for property get.
   * @return Never null
   */
  public static List<String> get(final BundleContext bundleContext, final ConfigKey... configKeys) {
    final List<String> list = new LinkedList<String>();
    for (final ConfigKey configKey : configKeys) {
      list.add(getProperty(bundleContext, configKey));
    }
    return list;
  }

  /**
   * Use {@link ConfigKey} paradigm to pull properties.
   * @param bundleContext Null not allowed
   * @param configKey Null not allowed
   * @return Never null
   */
  public static String getProperty(final BundleContext bundleContext, final ConfigKey configKey) {
    return bundleContext.getProperty(configKey.getKey());
  }

}
