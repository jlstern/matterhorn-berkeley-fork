/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.util.env;

import org.apache.commons.lang.StringUtils;

/**
 * @author John Crossman
 */
class ApplicationProfileMatcher implements EnumMatcher<ApplicationProfile> {

  private final Environment environment;
  private final boolean compareSubDomainToMatch;

  ApplicationProfileMatcher(final boolean compareSubDomainToMatch, final Environment environment) {
    this.compareSubDomainToMatch = compareSubDomainToMatch;
    this.environment = environment;
  }

  /**
   * @param arg expected to match one of {@link org.opencastproject.util.env.ApplicationProfile#values()}
   * @return null if name is null or does not match an {@link ApplicationProfile}. Otherwise, return the match.
   */
  public ApplicationProfile findMatch(final String arg) {
    final String name = StringUtils.isEmpty(arg) ? null : arg.trim().toLowerCase();
    ApplicationProfile profile = null;
    if (environment.equals(Environment.local)) {
      profile = ApplicationProfile.courses;
    } else if (name != null) {
      for (final ApplicationProfile nextProfile : ApplicationProfile.values()) {
        final String profileName = compareSubDomainToMatch ? nextProfile.getSubDomain() : nextProfile.name();
        if (StringUtils.startsWithIgnoreCase(name, profileName)) {
          profile = nextProfile;
          break;
        }
      }
    }
    return profile;
  }
}
