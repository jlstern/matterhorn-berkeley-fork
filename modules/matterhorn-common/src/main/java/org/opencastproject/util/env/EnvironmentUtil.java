/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.util.env;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.CharSetUtils;
import org.apache.commons.lang.StringUtils;
import org.opencastproject.util.ConfigurationException;
import org.opencastproject.util.MapUtils;
import org.opencastproject.util.XProperties;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * @author John Crossman
 */
public final class EnvironmentUtil {

  static final String matterhornHomeKey = "MATTERHORN_HOME";

  private static final Map<String, String> buildInformationMap = new HashMap<String, String>();

  /**
   * Absolute path to a Java properties file and it MUST exist. It will store:
   * <ul>
   *   <li>{@link PropertyOverride#matterhornEnvironment} (Required)</li>
   *   <li>{@link PropertyOverride#matterhornProfile} (Required)</li>
   *   <li>Passwords, etc. (Optional)</li>
   * </ul>
   * The Operations team will manage this file.
   */
  private static final String hostConfigurationsHome = "/opt/matterhorn";
  private static final String runtimePropertiesOverrideFile = hostConfigurationsHome + "/.mhruntime.cf";

  private static Dictionary<String, String> configProperties;

  private static Dictionary<String, String> systemProperties;

  /**
   * Singleton. For example, if this host machine is in production or QA.
   */
  private static Environment environmentSingleton;

  /**
   * Singleton. For example, if this host machine is in production or QA.
   */
  private static ApplicationProfile applicationProfileSingleton;

  /**
   * Singleton. Version number per Maven or similar version scheme.
   */
  private static String pomVersion;

  /**
   * Singleton. SVN revision number of current build.
   */
  private static String gitRevision;

  /**
   * Singleton. For example, see Bamboo build server scheme.
   */
  private static String buildNumber;

  /**
   * Singleton. Tells us, for example, if this host machine is in production or QA.
   */
  private static Dictionary<String, String> runtimePropertyOverrides;

  /**
   * Class-level initialization.
   */
  static {
    init();
  }

  /**
   * This class is strictly a set of static util methods. No need for instantiation.
   */
  private EnvironmentUtil() {
  }

  /**
   * @return Never null. {@link Environment} instance where name matches value of {@link System#getenv(String)}
   * with arg {@link PropertyOverride#matterhornEnvironment}. If {@link System#getenv(String)} returns null we also try
   * {@link System#getProperty(String)}.
   */
  public static Environment getEnvironment() {
    return environmentSingleton;
  }

  /**
   * @see #getEnvironment()
   * @return Never null.
   */
  public static ApplicationProfile getApplicationProfile() {
    return applicationProfileSingleton;
  }

  public static Dictionary<String, String> getRuntimePropertyOverrides() {
    final Dictionary<String, String> clone = new Hashtable<String, String>();
    final Enumeration<String> keys = runtimePropertyOverrides.keys();
    while (keys.hasMoreElements()) {
      final String key = keys.nextElement();
      clone.put(key, runtimePropertyOverrides.get(key));
    }
    return clone;
  }

  public static File getFileUnderFelixHome(final String relativePath) {
    return new File(getMatterhornHome(), relativePath);
  }

  public static String getMatterhornHome() {
    String envValue = System.getenv(matterhornHomeKey);
    envValue = (envValue == null) ? System.getProperty(matterhornHomeKey) : envValue;
    return (envValue == null) ? System.getenv("FELIX_HOME") : envValue;
  }

  public static String getBaseURL(final ApplicationProfile profile) {
    return getBaseURL(profile, EnvironmentUtil.getEnvironment());
  }

  /**
   * Package-local so unit test can vary the environment value.
   * @param profile Null not allowed
   * @param environment Null not allowed
   * @return never null
   */
  static String getBaseURL(final ApplicationProfile profile, final Environment environment) {
    return getHttpProtocol(profile) + "://" + getDomain(profile, environment);
  }

  private static String getDomain(final ApplicationProfile profile, final Environment environment) {
    final String baseURL;
    switch (environment) {
      case local:
        baseURL = "localhost:8080";
        break;
      case dev:
      case ci:
      case qa:
        baseURL = profile.getSubDomain() + '-' + environment.name() + '.' + getMatterhornDomain();
        break;
      case staging:
      case prod:
        baseURL = profile.getSubDomain() + '.' + getMatterhornDomain();
        break;
      default:
        throw new UnsupportedOperationException("No playback URL known for environment = " + environment);
    }
    return baseURL;
  }

  /**
   * Does the work of the constructor.
   */
  private static void init() {
    runtimePropertyOverrides = new Hashtable<String, String>();
    try {
      final File file = new File(runtimePropertiesOverrideFile);
      if (file.exists()) {
        final Properties properties = new XProperties();
        properties.load(new FileInputStream(file));
        final Enumeration<Object> keys = properties.keys();
        while (keys.hasMoreElements()) {
          final Object nextElement = keys.nextElement();
          // This type check should not be needed but better safe than sorry.
          if (nextElement instanceof String) {
            final String key = (String) nextElement;
            runtimePropertyOverrides.put(key, properties.getProperty(key));
          }
        }
      } else {
        throw new IOException(file.getAbsolutePath() + " is not readable.");
      }
    } catch (final IOException e) {
      throw new ConfigurationException("Failed to load file, which does exist: " + runtimePropertiesOverrideFile, e);
    }
    environmentSingleton = getProperty(runtimePropertyOverrides, PropertyOverride.matterhornEnvironment, new EnvironmentMatcher());
    applicationProfileSingleton = getProperty(runtimePropertyOverrides, PropertyOverride.matterhornProfile, new ApplicationProfileMatcher(false, getEnvironment()));
  }

  private static <T> T getProperty(final Dictionary<String, String> dictionary, final PropertyOverride propertyOverride, final EnumMatcher<T> enumMatcher) {
    final String name = dictionary.get(propertyOverride.getKey());
    T result = enumMatcher.findMatch(name);
    if (result == null) {
      throw new IllegalStateException("Invalid or missing property in "
          + runtimePropertiesOverrideFile
          + " for key = '"
          + propertyOverride.getKey()
          + "'. The unexpected value is '"
          + ((name == null) ? "[NULL]" : name)
          + "'.");
    }
    return result;
  }

  public static Environment findMatch(final String name) {
    return new EnvironmentMatcher().findMatch(name);
  }

  private static Dictionary<String, String> getRawProperties(final String path) {
    final Dictionary<String, String> result = new Hashtable<String, String>();
    try {
      final String filePath = StringUtils.remove(path, "file:").trim();
      final File file = new File(filePath);
      if (file.exists()) {
        final Properties properties = new Properties();
        properties.load(new FileInputStream(file));
        final Set<Object> objects = properties.keySet();
        for (final Object next : objects) {
          if (next instanceof String) {
            final String key = (String) next;
            result.put(key, properties.getProperty(key));
          }
        }
      }
    } catch (final IOException e) {
      e.printStackTrace();
    }
    return result;
  }

  /**
   * @param properties wrap this object.
   * @param injectSystemProperties when true, we inject Matterhorn's <i>config.properties</i>
   *                               and <i>system.properties</i>. This is useful because other property files can
   *                               reference, for example, "org.opencastproject.admin.ui.url" which is found in
   *                               config.properties file.
   * @return environment-aware properties object. See {@link EProperties}.
   */
  public static EProperties createEProperties(final Dictionary properties, boolean injectSystemProperties) {
    final EProperties result = new EProperties(properties);
    if (injectSystemProperties) {
      result.merge(getMatterhornConfigProperties());
      result.merge(getMatterhornSystemProperties());
    }
    return result;
  }

  /**
   * Package-local for sake of unit tests.
   * @return result of copy.
   */
  static Dictionary<String, String> getMatterhornSystemProperties() {
    if (systemProperties == null) {
      final Dictionary<String, String> d = new Hashtable<String, String>();
      final String property = System.getProperty("felix.system.properties");
      systemProperties = (property == null) ? d : copy(new EProperties(getRawProperties(property)), d);
    }
    return MapUtils.clone(systemProperties);
  }

  /**
   * Package-local for sake of unit tests.
   * @return result of copy.
   */
  public static Dictionary<String, String> getMatterhornConfigProperties() {
    if (configProperties == null) {
      final Dictionary<String, String> d = new Hashtable<String, String>();
      final String property = System.getProperty("felix.config.properties");
      configProperties = (property == null) ? d : copy(new EProperties(getRawProperties(property)), d);      
    }
    return MapUtils.clone(configProperties);
  }

  private static Dictionary<String, String> copy(final Properties source, final Dictionary<String, String> target) {
    final Set<Object> keySet = source.keySet();
    for (final Object next : keySet) {
      if (next instanceof String) {
        final String key = (String) next;
        target.put(key, source.getProperty(key));
      }
    }
    return target;
  }

  public static String getGitRevision() {
    if (gitRevision == null) {
      gitRevision = readBuildInformationFileToString("gitRevision");
    }
    return gitRevision;
  }

  public static String getPomVersion() {
    if (pomVersion == null) {
      pomVersion = readBuildInformationFileToString("pomVersion");
    }
    return pomVersion;
  }

  public static String getBambooBuild() {
    if (buildNumber == null) {
      buildNumber = readBuildInformationFileToString("bambooBuild");
    }
    return buildNumber;
  }

  /**
   * @return value of env variable: HOSTNAME
   */
  public static String getHostName() {
    return getDomain(getApplicationProfile(), getEnvironment());
  }

  private static String readBuildInformationFileToString(final String fileName) {
    final String defaultValue = "-1";
    try {
      final String result;
      if (buildInformationMap.containsKey(fileName)) {
        result = buildInformationMap.get(fileName);
      } else {
        final String relativePath = FilenameUtils.concat("lib/matterhorn/build-information", fileName);
        final File file = getFileUnderFelixHome(relativePath);
        if (file.exists()) {
          final String untrimmed = FileUtils.readFileToString(file);
          final String trimmed = CharSetUtils.delete(untrimmed, " \t\r\n\b");
          result = StringUtils.isEmpty(trimmed) ? defaultValue : trimmed;
        } else {
          result = defaultValue;
        }
        buildInformationMap.put(fileName, result);
      }
      return result;
    } catch (final IOException e) {
      e.printStackTrace();
      return defaultValue;
    }
  }

  public static String getMatterhornDomain() {
    return getRuntimePropertyOverrides().get("matterhorn.domain");
  }

  public static File getHostConfigurationsHome() {
    return new File(hostConfigurationsHome);
  }

  public static String getHttpProtocol(final ApplicationProfile profile) {
    final String protocol;
    if (profile.equals(getApplicationProfile())) {
      protocol = runtimePropertyOverrides.get(PropertyOverride.matterhornHttpProtocol.getKey());
    } else {
      protocol = "http";
    }
    return protocol;
  }

  public static ApplicationProfile getApplicationProfile(final String serverHostName) {
    return new ApplicationProfileMatcher(true, getEnvironment()).findMatch(serverHostName);
  }

  private static class EnvironmentMatcher implements EnumMatcher<Environment> {
    /**
     * @see #getEnvironment()
     * @param arg expected to match one of {@link org.opencastproject.util.env.Environment#values()}
     * @return null if name is null or does not match an {@link Environment}. Otherwise, return the match.
     */
    public Environment findMatch(final String arg) {
      final String name = StringUtils.isEmpty(arg) ? null : arg.trim().toLowerCase();
      Environment result = null;
      if (name != null) {
        for (final Environment e : Environment.values()) {
          if (name.equals(e.name())) {
            result = e;
            break;
          }
        }
      }
      return result;
    }
  }

}
