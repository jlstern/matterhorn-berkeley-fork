/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.util;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author John Crossman
 */
public class NumberUtilsTest {

  @Test
  public void testGetLongDefault() {
    assertEquals(100, NumberUtils.getLong(null, 100));
    assertEquals(100, NumberUtils.getLong("   ", 100));
  }

  @Test
  public void testGetLong() {
    assertEquals(1, NumberUtils.getLong(" 1 ", 100));
    assertEquals(100, NumberUtils.getLong("   100", 100000));
  }

  @Test(expected = NumberFormatException.class)
  public void testGetLongError() {
    NumberUtils.getLong("This is not a number, silly!", 100000);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testIllegalRange() {
    NumberUtils.range(5, 4);
  }

  @Test
  public void testRange() {
    final List<Integer> rangeWithOneEntry = NumberUtils.range(-5, -5);
    assertEquals(1, rangeWithOneEntry.size());
    assertEquals(-5, rangeWithOneEntry.get(0).intValue());
    //
    final List<Integer> rangeWithElevenEntries = NumberUtils.range(0, 10);
    assertEquals(11, rangeWithElevenEntries.size());
    assertEquals(0, rangeWithElevenEntries.get(0).intValue());
    assertEquals(5, rangeWithElevenEntries.get(5).intValue());
    assertEquals(10, rangeWithElevenEntries.get(10).intValue());
  }
}
