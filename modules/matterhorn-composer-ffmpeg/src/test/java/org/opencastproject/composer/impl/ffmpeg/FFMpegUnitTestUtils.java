/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.composer.impl.ffmpeg;

import static org.junit.Assert.assertTrue;

import org.opencastproject.mediapackage.MediaPackageElementParser;
import org.opencastproject.mediapackage.MediaPackageException;
import org.opencastproject.mediapackage.Track;
import org.opencastproject.util.FileSupport;
import org.opencastproject.util.env.EnvironmentUtil;

import java.io.File;
import java.io.IOException;

/**
 * @author John Crossman
 */
public final class FFMpegUnitTestUtils {

  private static final String presenterTrackXml = "<track id=\"track-1\" type=\"presentation/source\"><mimetype>video/quicktime</mimetype>"
      + "<url>http://localhost:8080/workflow/samples/camera.mpg</url>"
      + "<checksum type=\"md5\">43b7d843b02c4a429b2f547a4f230d31</checksum><duration>14546</duration>"
      + "<video><device type=\"UFG03\" version=\"30112007\" vendor=\"Unigraf\" />"
      + "<encoder type=\"H.264\" version=\"7.4\" vendor=\"Apple Inc\" /><resolution>640x480</resolution>"
      + "<scanType type=\"progressive\" /><bitrate>540520</bitrate><frameRate>2</frameRate></video></track>";

  private static final String presentationTrackXml = "<track id=\"track-1\" type=\"presentation/source\"><mimetype>video/quicktime</mimetype>"
      + "<url>http://localhost:8080/workflow/samples/screen.mpg</url>"
      + "<checksum type=\"md5\">43b7d843b02c4a429b2f547a4f230d31</checksum><duration>14546</duration>"
      + "<video><device type=\"UFG03\" version=\"30112007\" vendor=\"Unigraf\" />"
      + "<encoder type=\"H.264\" version=\"7.4\" vendor=\"Apple Inc\" /><resolution>640x480</resolution>"
      + "<scanType type=\"progressive\" /><bitrate>540520</bitrate><frameRate>2</frameRate></video></track>";

  private FFMpegUnitTestUtils() {
  }

  public static File getEncodingprofilesProperties() throws IOException {
    return getFile("modules/matterhorn-composer-ffmpeg/src/test/resources", "encodingprofiles.properties");
  }

  public static File getFile(final String relativePath, String fileName) throws IOException {
    final File directory = new File(EnvironmentUtil.getMatterhornHome(), relativePath);
    final File file = new File(directory, fileName);
    assertTrue("File not found: " + file.getAbsolutePath(), file.exists());
    return FileSupport.copy(file, FileSupport.getTempDirectory(), true);
  }

  public static Track getSamplePresenterTrack() throws MediaPackageException {
    return (Track) MediaPackageElementParser.getFromXml(presenterTrackXml);
  }

  public static Track getSamplePresentationTrack() throws MediaPackageException {
    return (Track) MediaPackageElementParser.getFromXml(presentationTrackXml);
  }

  public static File getSamplePresenterVideoFile() throws IOException {
    return getFile("modules/matterhorn-workflow-service-impl/src/main/resources/sample", "camera.mpg");
  }

  public static File getSamplePresentationVideoFile() throws IOException {
    return getFile("modules/matterhorn-workflow-service-impl/src/main/resources/sample", "screen.mpg");
  }

}
