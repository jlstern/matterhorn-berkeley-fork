/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.kernel.mail;

/**
 * @author John Crossman
 */
public enum TeamAddress {

  grizzlyPeakDevelopers("Grizzly Peak Developers", "grizzly-peak-developers@lists.berkeley.edu"),
  grizzlyPeakErrors("Grizzly Peak Errors", "grizzly-peak-error@lists.berkeley.edu"),
  grizzlyPeakSalesforce("Grizzly Peak Salesforce", "webcast@berkeley.edu"),;

  private final String recipientName;
  private final String address;

  private TeamAddress(final String recipientName, final String address) {
    this.recipientName = recipientName;
    this.address = address;
  }

  String getRecipientName() {
    return recipientName;
  }

  String getAddress() {
    return address;
  }


  @Override
  public String toString() {
    return getRecipientName() + " <" + getAddress() + ">";
  }
}