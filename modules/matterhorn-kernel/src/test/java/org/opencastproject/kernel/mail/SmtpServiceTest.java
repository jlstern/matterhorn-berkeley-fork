/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.kernel.mail;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.opencastproject.notify.SendType;
import org.opencastproject.util.env.EnvironmentUtil;

import org.junit.Before;
import org.junit.Test;
import org.osgi.service.cm.ConfigurationException;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * @author John Crossman
 */
public class SmtpServiceTest {

  private MockSmtpService smtpService;

  @Before
  public void before() throws ConfigurationException {
    smtpService = new MockSmtpService();
  }

  @Test
  public void testNotifyEngineeringTeamDev() throws MessagingException {
    smtpService.overrideDefaultSendMode(SendMode.divertWhenNecessary);
    final SendType sendType = smtpService.notifyEngineeringTeam("hello", "world");
    assertEquals(SendType.messageSent, sendType);
    final MimeMessage messageSent = smtpService.getLastMessageSent();
    final String subject = messageSent.getSubject();
    assertTrue(subject.startsWith("["));
    assertTrue(subject.contains("hello"));
  }

  @Test
  public void testLogEmailWhenLocalEnv() throws MessagingException {
    smtpService.overrideDefaultSendMode(SendMode.neverSend);
    final String subject = "Hello World";
    final SendType sendType = smtpService.notifyEngineeringTeam(new Exception(subject));
    assertEquals(SendType.messageLoggedNotSent, sendType);
    assertProcessedSubjectLine(subject, smtpService.getLastMessageSent());
  }

  @Test
  public void testDivertNecessary() throws MessagingException {
    smtpService.overrideDefaultSendMode(SendMode.divertWhenNecessary);
    final MimeMessage message = getMessage("instructor@berkeley.edu");
    final String subject = message.getSubject();
    final SendType sendType = smtpService.send(message);
    assertEquals(SendType.messageDiverted, sendType);
    assertProcessedSubjectLine(subject, smtpService.getLastMessageSent());
  }

  @Test
  public void testDivertNotNecessary() throws MessagingException {
    smtpService.overrideDefaultSendMode(SendMode.divertWhenNecessary);
    final String subject = "Hello World";
    final SendType sendType = smtpService.notifyEngineeringTeam(new Exception(subject));
    assertEquals(SendType.messageSent, sendType);
    assertProcessedSubjectLine(subject, smtpService.getLastMessageSent());
  }

  @Test
  public void testDivertFromTeamToDevelopers() throws MessagingException {
    smtpService.overrideDefaultSendMode(SendMode.divertWhenNecessary);
    final MimeMessage message = getMessage(TeamAddress.grizzlyPeakErrors.getAddress());
    final String subject = message.getSubject();
    final SendType sendType = smtpService.send(message);
    assertEquals(SendType.messageSent, sendType);
    assertProcessedSubjectLine(subject, smtpService.getLastMessageSent());
  }

  @Test
  public void testProduction() throws MessagingException {
    smtpService.overrideDefaultSendMode(SendMode.productionMode);
    final MimeMessage message = getMessage("instructor@berkeley.edu");
    final String subject = message.getSubject();
    assertEquals(SendType.messageSent, smtpService.send(message));
    assertEquals(subject, smtpService.getLastMessageSent().getSubject());
  }

  @Test
  public void testProductionEngineeringTeam() throws MessagingException {
    smtpService.overrideDefaultSendMode(SendMode.productionMode);
    final String subject = "Subject line";
    assertEquals(SendType.messageSent, smtpService.notifyEngineeringTeam(subject, "body text"));
    assertProcessedSubjectLine(subject, smtpService.getLastMessageSent());
  }

  @Test
  public void testTestEmailAtStartup() throws MessagingException, ConfigurationException {
    smtpService.overrideDefaultSendMode(SendMode.neverSend);
    final Properties p = new Properties();
    p.put("mail.test", "true");
    smtpService.updatedConfiguration(p);
    final MimeMessage messageSent = smtpService.getLastMessageSent();
    assertNotNull(messageSent);
    assertTrue(messageSent.getSubject().startsWith("["));
  }

  private void assertProcessedSubjectLine(final String subject, final MimeMessage message) throws MessagingException {
    final String[] expectedFragments = new String[] {
        EnvironmentUtil.getPomVersion(),
        EnvironmentUtil.getGitRevision(),
        EnvironmentUtil.getBambooBuild(),
        subject
    };
    final String processedSubject = message.getSubject();
    for (final String expected : expectedFragments) {
      assertTrue("'" + expected + "' not found in " + processedSubject, processedSubject.contains(expected));
    }
  }

  private MimeMessage getMessage(final String address) throws MessagingException {
    final MimeMessage message = smtpService.createMessage();
    message.setSubject("Unique subject line: " + message.hashCode());
    message.setRecipient(Message.RecipientType.TO, new InternetAddress(address));
    return message;
  }

}
