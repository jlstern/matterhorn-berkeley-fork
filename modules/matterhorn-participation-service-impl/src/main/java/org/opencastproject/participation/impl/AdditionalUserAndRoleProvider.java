/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl;

import org.apache.commons.lang.StringUtils;
import org.opencastproject.job.api.EManagedService;
import org.opencastproject.security.api.RoleProvider;
import org.opencastproject.security.api.User;
import org.opencastproject.security.api.UserProvider;
import org.opencastproject.util.OsgiUtil;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

public class AdditionalUserAndRoleProvider extends EManagedService implements UserProvider, RoleProvider {

  /** The logger */
  private static final Logger logger = LoggerFactory.getLogger(AdditionalUserAndRoleProvider.class);

  private static final String USER_ROLE = "ROLE_USER";

  private static final String ADMIN_ROLE = "ROLE_ADMIN";

  private static final String ORG_KEY = "org.opencastproject.additionalusers.org";

  private static final String ADMIN_USERS_KEY = "org.opencastproject.additionalusers.admin";

  private static final String USER_USERS_KEY = "org.opencastproject.additionalusers.user";

  private final Timer configFileReaderTimer = new Timer(true);

  private String organization = null;

  private final Map<String, User> users = Collections.synchronizedMap(new HashMap<String, User>());

  public AdditionalUserAndRoleProvider() {
  }

  @Override
  public String[] getRoles() {
    return new String[] { USER_ROLE, ADMIN_ROLE };
  }

  @Override
  public String[] getRolesForUser(final String userName) {
    // all the other providers do this to prevent NPE,
    // maybe roles are just retrieved from User instance
    // seems like the API should change to eliminate this method
    return new String[0];
  }

  @Override
  public User loadUser(final String userName) {
    return this.users.get(userName);
  }

  @Override
  public String getOrganization() {
    return this.organization;
  }

  /**
   * {@inheritDoc}
   *
   * @see org.osgi.service.cm.ManagedService#updated(java.util.Dictionary)
   */
  @Override
  public void updatedConfiguration(final Properties d) throws ConfigurationException {
    organization = OsgiUtil.getCfg(d, ORG_KEY);
    //
    final TimerTask task = new TimerTask() {
      @Override
      public void run() {
        try {
          final Properties properties = new Properties();
          properties.load(new FileReader(new File("/opt/matterhorn/.mhruntime.cf")));
          final String usersWithAdminRole = properties.getProperty(ADMIN_USERS_KEY);
          final String usersWithUserRole = properties.getProperty(USER_USERS_KEY);
          final String[] admins = StringUtils.split(usersWithAdminRole, ',');
          final String[] users = StringUtils.split(usersWithUserRole, ',');
          makeUsers(admins == null ? new String[0] : admins, users == null ? new String[0] : users);
          logger.debug("Assigned ROLE_ADMIN: {}" + '\n' + "Assigned ROLE_USER: {}", new Object[] {usersWithAdminRole, usersWithUserRole});
        } catch (final Exception e) {
          throw new RuntimeException(e);
        }
      }
    };
    final int minutesFrequency = 1;
    configFileReaderTimer.scheduleAtFixedRate(task, 0, minutesFrequency * 60000);
  }

  private void makeUsers(final String[] adminUsers, final String[] userUsers) {
    int initialCapacity = adminUsers.length >= userUsers.length ? adminUsers.length : userUsers.length;
    final Map<String, Set<String>> userRoles = new HashMap<String, Set<String>>(initialCapacity);
    Set<String> thisUserRoles;
    for (final String adminUserName : adminUsers) {
      thisUserRoles = userRoles.get(adminUserName);
      if (thisUserRoles == null) {
        thisUserRoles = new HashSet<String>();
        thisUserRoles.add(ADMIN_ROLE);
        userRoles.put(adminUserName, thisUserRoles);
      } else {
        thisUserRoles.add(ADMIN_ROLE);
      }
    }
    for (final String userUserName : userUsers) {
      thisUserRoles = userRoles.get(userUserName);
      if (thisUserRoles == null) {
        thisUserRoles = new HashSet<String>();
        thisUserRoles.add(USER_ROLE);
        userRoles.put(userUserName, thisUserRoles);
      } else {
        thisUserRoles.add(USER_ROLE);
      }
    }
    this.users.clear();
    final Set<Entry<String, Set<String>>> userEntries = userRoles.entrySet();
    for (Entry<String, Set<String>> userEntry : userEntries) {
      final String thisUserName = userEntry.getKey();
      if (StringUtils.isNotBlank(thisUserName)) {
        thisUserRoles = userEntry.getValue();
        final User thisUser = new User(thisUserName, getOrganization(), thisUserRoles.toArray(new String[thisUserRoles.size()]));
        logger.debug("created user: {} in Organization: {} with roles: {}", new Object[]{thisUserName, getOrganization(), thisUserRoles});
        this.users.put(thisUserName, thisUser);
      }
    }
  }

  public void activate(final ComponentContext cc) throws Exception {
    logger.debug("Activating AdditionalUserAndRoleProvider Service");
  }

  /**
   * Deactivates Participation Service. 
   */
  public void deactivate(final ComponentContext cc) throws Exception {
    logger.debug("Deactivating AdditionalUserAndRoleProvider Service");
    this.users.clear();
    this.organization = null;
  }

}
