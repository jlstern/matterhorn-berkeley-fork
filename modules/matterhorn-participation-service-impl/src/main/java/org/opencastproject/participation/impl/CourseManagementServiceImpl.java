/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl;

import org.opencastproject.job.api.EManagedService;
import org.opencastproject.notify.Notifier;
import org.opencastproject.participation.api.CourseManagementService;
import org.opencastproject.participation.impl.persistence.DatabaseCredentials;
import org.opencastproject.participation.impl.persistence.FileBasedCourseCatalogData;
import org.opencastproject.participation.impl.persistence.ProvidesCourseCatalogData;
import org.opencastproject.participation.impl.persistence.UCBerkeleyCourseDatabaseImpl;
import org.opencastproject.participation.model.CapturePreferences;
import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.CourseOffering;
import org.opencastproject.participation.model.Instructor;
import org.opencastproject.participation.model.Participation;
import org.opencastproject.participation.model.Room;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.participation.model.Term;
import org.opencastproject.salesforce.HasSalesforceFieldName;
import org.opencastproject.salesforce.RepresentsSalesforceField;
import org.opencastproject.salesforce.SalesforceConnectionException;
import org.opencastproject.salesforce.SalesforceConnectorService;
import org.opencastproject.salesforce.SalesforceFieldInstructor;
import org.opencastproject.salesforce.SalesforceIndexOutOfBoundsException;
import org.opencastproject.salesforce.SalesforceObjectTransformer;
import org.opencastproject.salesforce.SalesforceObjectTransformerImpl;
import org.opencastproject.salesforce.SalesforceOperationException;
import org.opencastproject.salesforce.SalesforceQueryType;
import org.opencastproject.salesforce.SalesforceUtils;
import org.opencastproject.salesforce.entity.Contact;
import org.opencastproject.salesforce.entity.CourseField;
import org.opencastproject.salesforce.entity.RecordType;
import org.opencastproject.util.NotFoundException;
import org.opencastproject.util.NumberUtils;
import org.opencastproject.util.data.Collections;
import org.opencastproject.util.date.DateUtil;
import org.opencastproject.util.date.TimeOfDay;
import org.opencastproject.util.env.ApplicationProfile;
import org.opencastproject.util.env.Environment;
import org.opencastproject.util.env.EnvironmentUtil;

import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.SaveResult;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectionException;

import org.apache.commons.lang.StringUtils;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Timer;

public class CourseManagementServiceImpl extends EManagedService implements CourseManagementService {

  public CourseManagementServiceImpl() {
  }

  public CourseManagementServiceImpl(final Logger logger) {
    this.logger = logger;
  }

  public void activate(final ComponentContext componentContext) throws Exception {
    logger.info("Activating CourseManagementServiceImpl Service");
  }

  @Override
  protected void updatedConfiguration(final Properties properties) throws ConfigurationException {
    // Note the idiom here is to wrap all specific exceptions in a ConfigurationException
    try {
      if (properties == null || properties.isEmpty()) {
        throw new IllegalStateException("Missing valid Properties file");
      }
      logger.info("Updating Course Management Service with properties containing {} keys", properties.size());

      if (ApplicationProfile.courses == EnvironmentUtil.getApplicationProfile()) {

        // Run either file or view-based CourseDataMover depending on the
        // environment we're in and the properties settings
        if (parsePropertiesToEnvironments(properties, FILE_BASED_ENVIRONMENTS).contains(getEnvironment())) {
          startFileWatchingCourseDataMover();
        } else {
          if (parsePropertiesToEnvironments(properties, VIEW_BASED_ENVIRONMENTS).contains(getEnvironment())) {
            scheduleCourseDataMover(properties);
          } else {
            throw new UnsupportedOperationException("CourseDataMover is not supported in '" + getEnvironment()
                    + "' environment");
          }
        }
      }
    } catch (final Throwable e) {
      final ConfigurationException ce = new ConfigurationException(CourseDataMover.class.getSimpleName(),
              "Failure prior to scheduling of Course Data Mover service ");
      ce.initCause(e);
      notifier.notifyEngineeringTeam(ce);
      throw ce;
    }
  }

  @Override
  public CourseOffering getCourseOffering(final String courseOfferingId) {
    final String id = StringUtils.trimToNull(courseOfferingId);
    final SObject sObject = isValidCourseOfferingId(id) ? getRecord(SalesforceQueryType.selectCourseOffering,
            Key.courseOfferingId.name(), courseOfferingId) : null;
    return (sObject == null) ? null : salesforceObjectTransformer.getCourseOffering(sObject);
  }

  private boolean isValidCourseOfferingId(final String id) {
    return id != null && id.length() < 20;
  }

  @Override
  public Instructor getInstructorByCalNetUID(final String calNetUID) throws NotFoundException {
    final SObject sObject = getRecord(SalesforceQueryType.selectInstructorByCalNetUID, Key.calNetUID.name(), calNetUID);
    return (sObject == null) ? null : salesforceObjectTransformer.getInstructor(sObject);
  }

  @Override
  public boolean updateCapturePreferences(final String courseOfferingId, final String calNetUID,
          final CapturePreferences capturePreferences) throws NotFoundException {
    final boolean recordingsScheduled = courseRecordingsScheduled(courseOfferingId);
    if (recordingsScheduled) {
      approveCapturePreferences(courseOfferingId, calNetUID);
    } else {
      final Map<HasSalesforceFieldName, Object> updates = new HashMap<HasSalesforceFieldName, Object>();
      updates.put(CourseField.Recording_Type__c, getSalesforceValue(capturePreferences.getRecordingType()));

      updates.put(CourseField.Recording_Availability__c,
              getSalesforceValue(capturePreferences.getRecordingAvailability()));
      updates.put(CourseField.Publish_Delay_Days__c, Integer.toString(capturePreferences.getDelayPublishByDays()));

      final CourseOffering courseOffering = getCourseOffering(courseOfferingId);
      final HasSalesforceFieldName approvalFieldName = SalesforceUtils.getInstructorApprovalFieldName(courseOffering,
              calNetUID);
      updates.put(approvalFieldName, Boolean.TRUE);
      //
      final Map<String, String> args = new HashMap<String, String>();
      args.put(Key.courseOfferingId.name(), courseOfferingId);
      args.put(Key.instructorFieldName.name(), approvalFieldName.name());
      update(SalesforceQueryType.selectCapturePreferences, args, updates);
    }
    // See javadoc on this method to understand purpose of return value.
    return !recordingsScheduled && allInstructorsApprove(getCourseOffering(courseOfferingId).getParticipationSet());
  }

  @Override
  public boolean updateCapturePreferencesByAdmin(final String courseOfferingId, final String adminCalNetUID,
          final CapturePreferences capturePreferences) throws NotFoundException {
    final boolean recordingsScheduled = courseRecordingsScheduled(courseOfferingId);
    if (recordingsScheduled) {
      throw new IllegalStateException("Recordings are already scheduled for course " + courseOfferingId
              + ". Updating capture preferences is not allowed.");
    }
    final Map<HasSalesforceFieldName, Object> updates = new HashMap<HasSalesforceFieldName, Object>();
    updates.put(CourseField.Recording_Type__c, getSalesforceValue(capturePreferences.getRecordingType()));

    updates.put(CourseField.Recording_Availability__c,
            getSalesforceValue(capturePreferences.getRecordingAvailability()));
    updates.put(CourseField.Publish_Delay_Days__c, Integer.toString(capturePreferences.getDelayPublishByDays()));

    updates.put(CourseField.admin_schedule_override__c, adminCalNetUID);
    //
    final Map<String, String> args = new HashMap<String, String>();
    args.put(Key.courseOfferingId.name(), courseOfferingId);
    update(SalesforceQueryType.selectCapturePreferencesByAdmin, args, updates);
    // an approval override by an admin means recordings are ready to be scheduled with the
    return true;
  }

  @Override
  public void approveCapturePreferences(final String courseOfferingId, final String calNetUID) throws NotFoundException {
    final CourseOffering courseOffering = getCourseOffering(courseOfferingId);
    final HasSalesforceFieldName approvalFieldName = SalesforceUtils.getInstructorApprovalFieldName(courseOffering,
            calNetUID);
    //
    final Map<HasSalesforceFieldName, Object> updates = new HashMap<HasSalesforceFieldName, Object>();
    updates.put(approvalFieldName, Boolean.TRUE);
    //
    final Map<String, String> args = new HashMap<String, String>();
    args.put(Key.courseOfferingId.name(), courseOfferingId);
    args.put(Key.instructorFieldName.name(), approvalFieldName.name());
    update(SalesforceQueryType.selectInstructorCapturePreferences, args, updates);
  }

  @Override
  public void setRecordingsScheduledTrue(final String courseOfferingId) throws NotFoundException {
    final Map<HasSalesforceFieldName, Object> updates = new HashMap<HasSalesforceFieldName, Object>();
    updates.put(CourseField.Recordings_Scheduled__c, Boolean.TRUE);
    //
    final Map<String, String> args = new HashMap<String, String>();
    args.put(Key.courseOfferingId.name(), courseOfferingId);
    update(SalesforceQueryType.selectRecordingsScheduledTrue, args, updates);
  }

  @Override
  public void createOrUpdateCourses(final Set<CourseData> courseDataSet) {
    if (courseDataSet.size() > 0) {
      final Map<Integer, Map<Semester, List<CourseData>>> organized = organize(courseDataSet);
      for (final Integer year : organized.keySet()) {
        final Map<Semester, List<CourseData>> semesterCourseMap = organized.get(year);
        for (final Semester semester : semesterCourseMap.keySet()) {
          final Map<CourseOffering, SObject> mapFromSalesforce = getCourseOfferingMapBySemesterYear(semester, year);
          for (final CourseData courseData : courseDataSet) {
            final CourseOffering matchingCourseFromSalesforce = getMatchingCourseFromSalesforce(
                    mapFromSalesforce.keySet(), courseData);
            validate(courseData);
            final String courseOfferingID = year + semester.getTermCode() + courseData.getCanonicalCourse().getCcn();
            if (matchingCourseFromSalesforce != null) {
              try {
                logger.info("Salesforce: Update course: " + courseOfferingID);
                setParticipationApprovalAndPosition(courseData, matchingCourseFromSalesforce.getParticipationSet());
                updateCourse(mapFromSalesforce.get(courseData), courseData);
              } catch (final SalesforceIndexOutOfBoundsException e) {
                logger.warn(courseData.toString(), e);
              }
            } else {
              logger.info("Salesforce: Creating course: " + courseOfferingID);
              setParticipationApprovalAndPosition(courseData, null);
              createCourse(courseData);
            }
          }
        }
      }
    }
  }

  @Override
  public List<Instructor> createOrUpdateInstructors(final Set<Instructor> incomingInstructorSet) {
    final Map<Instructor, SObject> fromSalesforceMap = getInstructorSObjectMap();
    for (final Instructor instructor : incomingInstructorSet) {
      final SObject instructorSObject = fromSalesforceMap.get(instructor);
      final String salesforceID;
      //TODO this causes a ClassNotFoundException due to a jar not being available
      // Commenting out for now since it is not essential
      //final String emailAddress = EmailValidator.getInstance().isValid(instructor.getEmail()) ? SalesforceUtils.getSafeEmailAddress(instructor) : null;
      final String emailAddress = SalesforceUtils.getSafeEmailAddress(instructor);
      
      if (instructorSObject == null) {
        final SObject sObject = new SObject();
        sObject.setType(RecordType.contact.getSalesforceValue());
        SalesforceUtils.setField(sObject, Contact.Calnet_UID__c, instructor.getCalNetUID());
        SalesforceUtils.setField(sObject, Contact.FirstName, instructor.getFirstName());
        SalesforceUtils.setField(sObject, Contact.LastName, instructor.getLastName());
        if (emailAddress == null) {
          logger.warn("instructor calNetUID = " + instructor.getCalNetUID()
                  + " has null email in UC Berkeley catalog database");
        } else {
          SalesforceUtils.setField(sObject, Contact.Email, emailAddress);
        }
        SalesforceUtils.setField(sObject, Contact.Role__c, "Faculty");
        SalesforceUtils.setField(sObject, Contact.Department, instructor.getDepartment());
        salesforceID = createInstructor(instructor.getCalNetUID(), sObject);
      } else {
        salesforceID = instructorSObject.getId();
        //
        final List<Instructor> list = new ArrayList<Instructor>(fromSalesforceMap.keySet());
        final Instructor instructorFromSalesforce = list.get(list.indexOf(instructor));
        if (updateSalesforceRecord(instructor, instructorFromSalesforce)) {
          SalesforceUtils.setField(instructorSObject, Contact.FirstName, instructor.getFirstName());
          SalesforceUtils.setField(instructorSObject, Contact.LastName, instructor.getLastName());
          if (emailAddress == null) {
            logger.warn("instructor calNetUID = " + instructor.getCalNetUID()
                    + " has null email in UC Berkeley catalog database");
          } else {
            SalesforceUtils.setField(instructorSObject, Contact.Email, emailAddress);
          }
          SalesforceUtils.setField(instructorSObject, Contact.Department, instructor.getDepartment());
          update(instructorSObject);
        }
      }
      if (salesforceID == null) {
        throw new IllegalStateException("Null salesforceId from instructor: " + instructor);
      }
      instructor.setSalesforceID(salesforceID);
    }
    return new LinkedList<Instructor>(incomingInstructorSet);
  }

  private String createInstructor(final String calNetUID, final SObject sObject) {
    String salesforceId;
    try {
      final SaveResult saveResult = create(sObject);
      salesforceId = saveResult.getId();
    } catch (final Exception e) {
      final String message = e.getMessage();
      final boolean existingRecord = StringUtils.contains(message, "duplicate value found")
              && StringUtils.contains(message, "Calnet_UID__c");
      if (existingRecord) {
        try {
          final Instructor instructor = getInstructorByCalNetUID(calNetUID);
          salesforceId = instructor.getSalesforceID();
        } catch (final NotFoundException e1) {
          throw new SalesforceOperationException("Failed to create Instructor " + calNetUID + " with salesforceId = "
                  + SalesforceUtils.toString(sObject), e1);
        }
      } else {
        throw new SalesforceOperationException("Failed to create Instructor " + calNetUID + " with salesforceId = "
                + SalesforceUtils.toString(sObject), e);
      }
    }
    if (salesforceId == null) {
      throw new SalesforceOperationException("Failed to create Instructor " + calNetUID + " with salesforceId = "
              + SalesforceUtils.toString(sObject));
    }
    return salesforceId;
  }

  @Override
  public Set<Term> getAllTerms() {
    final Set<Term> set = new HashSet<Term>();
    final SObject[] records = getRecords(SalesforceQueryType.selectAllTerms, null);
    for (final SObject record : records) {
      final Term term = salesforceObjectTransformer.getTerm(record);
      if (set.contains(term)) {
        throw new SalesforceOperationException("Salesforce has duplicate records for term: " + term);
      }
      set.add(term);
    }
    return set;
  }

  @Override
  public Set<Room> getAllRooms() {
    final Set<Room> set = new HashSet<Room>();
    final SObject[] records = getRecords(SalesforceQueryType.selectAllRooms, null);
    for (final SObject record : records) {
      final Room room = salesforceObjectTransformer.getRoom(record);
      if (set.contains(room)) {
        throw new SalesforceOperationException("Salesforce has duplicate records for room: " + room);
      }
      set.add(room);
    }
    return set;
  }

  @Override
  public boolean courseRecordingsScheduled(String courseOfferingId) {
    final SObject record = getRecord(SalesforceQueryType.selectRecordingsScheduledTrue, Key.courseOfferingId.name(),
            courseOfferingId);
    return salesforceObjectTransformer.isRecordingsScheduled(record);
  }

  @Override
  public String getCaptureAgentName(final String courseOfferingId) {
    final CourseOffering courseOffering = getCourseOffering(courseOfferingId);
    final Room room = (courseOffering == null) ? null : courseOffering.getRoom();
    return (room == null) ? null : getCaptureAgentName(room);
  }

  /**
   * @param room
   *          Null not allowed
   * @return all lowercase, spaces replaced by dashes.
   */
  private String getCaptureAgentName(final Room room) {
    final String buildingName = StringUtils.replaceChars(room.getBuilding().toLowerCase(), ' ', '-');
    return buildingName + '-' + room.getRoomNumber();
  }

  @Override
  public CapturePreferences getCapturePreferences(final String courseOfferingId) {
    final CourseOffering courseOffering = getCourseOffering(courseOfferingId);
    return (courseOffering == null) ? null : courseOffering.getCapturePreferences();
  }

  void scheduleCourseDataMover(final Properties properties) throws IOException {
    // Start cron job which imports from course data from materialized view
    final String hostname = properties.getProperty("db.courseCatalog.hostname");
    final int port = Integer.parseInt(properties.getProperty("db.courseCatalog.port"));
    final String databaseName = properties.getProperty("db.courseCatalog.databaseName");
    final String username = properties.getProperty("db.courseCatalog.username");
    final String password = properties.getProperty("db.courseCatalog.password");
    final DatabaseCredentials dbCredentials = new DatabaseCredentials(hostname, port, databaseName, username, password);
    final ProvidesCourseCatalogData courseDatabase = new UCBerkeleyCourseDatabaseImpl(dbCredentials);
    final CourseDataMover dataMover = new CourseDataMover();
    dataMover.setCourseDatabase(courseDatabase);
    dataMover.setCourseManagementService(this);
    dataMover.setNotifier(notifier);
    //
    final String startTimeMilitary = properties.getProperty("dataMover.cron.startTime");
    final TimeOfDay timeOfDay = DateUtil.getTimeOfDay(startTimeMilitary);
    final long intervalMinutes = NumberUtils.getLong(properties.getProperty("dataMover.cron.intervalMinutes"), 1440);
    databaseLookupTimer.scheduleAtFixedRate(dataMover, DateUtil.getTodayDate(timeOfDay), intervalMinutes * 60000);
    logger.info(dataMover.getClass().getSimpleName() + " (" + courseDatabase.getClass().getSimpleName()
            + ") will run once every " + intervalMinutes + " minutes, starting at " + startTimeMilitary
            + " military time.");
  }

  /**
   * Updates based on what is currently set in Salesforce. For example, instructor approval of course capture.
   * 
   * @param courseData
   *          pulled from remote UCB catalog data source.
   * @param fromSalesforce
   *          when not null, the set pulled from Salesforce where CCN and term match incoming course
   */
  private void setParticipationApprovalAndPosition(final CourseData courseData, final Set<Participation> fromSalesforce) {
    final List<String> approvalsList = new LinkedList<String>();
    final Map<String, SalesforceFieldInstructor> salesforceIndexMap = new HashMap<String, SalesforceFieldInstructor>();
    if (fromSalesforce != null) {
      for (final Participation p : fromSalesforce) {
        final String salesforceID = p.getInstructor().getSalesforceID();
        if (p.isApproved()) {
          approvalsList.add(salesforceID);
        }
        salesforceIndexMap.put(salesforceID, p.getSalesforceField());
      }
    }
    final Set<Participation> participationSet = courseData.getParticipationSet();
    for (final Participation p : participationSet) {
      final String salesforceID = p.getInstructor().getSalesforceID();
      p.setApproved(approvalsList.contains(salesforceID));
      p.setSalesforceField(salesforceIndexMap.get(salesforceID));
    }
    final SalesforceFieldInstructor[] availableFields = SalesforceUtils.getAvailableInstructorFields(participationSet);
    int nextAvailable = 0;
    for (final Participation p : participationSet) {
      if (p.getSalesforceField() == null) {
        p.setSalesforceField(availableFields[nextAvailable++]);
      }
    }
  }

  /**
   * @param fromSalesforceSet
   *          the set to scan
   * @param incoming
   *          what we are matching on
   * @return Null when no match found.
   */
  private CourseOffering getMatchingCourseFromSalesforce(final Set<CourseOffering> fromSalesforceSet,
          final CourseData incoming) {
    CourseOffering result = null;
    for (final CourseOffering courseOffering : fromSalesforceSet) {
      if (incoming.equals(courseOffering)) {
        result = courseOffering;
        break;
      }
    }
    return result;
  }

  void startFileWatchingCourseDataMover() {
    final CourseDataMover dataMover = new CourseDataMover();
    final FileBasedCourseCatalogData courseDatabase = new FileBasedCourseCatalogData("/opt/matterhorn/catalog-data");
    dataMover.setCourseDatabase(courseDatabase);
    dataMover.setCourseManagementService(this);
    dataMover.setNotifier(notifier);
    //
    final int milliseconds = 120000;
    filesystemLookupTimer.scheduleAtFixedRate(dataMover, milliseconds, milliseconds);
    logger.info(dataMover.getClass().getSimpleName() + " (" + courseDatabase.getClass().getSimpleName()
            + ") will run once every " + milliseconds + " milliseconds.");
  }

  /*
   * package scope for unit testing
   * 
   * @param participationSet one or more instructor approval flags
   * 
   * @return true if all are {@link org.opencastproject.participation.model.Participation#isApproved()}
   */
  boolean allInstructorsApprove(final Set<Participation> participationSet) {
    boolean allApprove = true;
    for (final Participation participation : participationSet) {
      if (!participation.isApproved()) {
        allApprove = false;
        break;
      }
    }
    return allApprove;
  }

  private void updateCourse(final SObject sObject, final CourseData courseData) {
    final boolean neverUpdate = NEVER_UPDATE_COURSES_IN_SALESFORCE.contains(courseData.getCanonicalCourse().getCcn());
    if (!neverUpdate) {
      salesforceObjectTransformer.setFieldsForCourseUpdate(sObject, courseData);
      update(sObject);
    }
  }

  private void createCourse(final CourseData courseData) {
    final SObject sObject = salesforceObjectTransformer.getSObjectForCourseCreation(courseData);
    create(sObject);
  }

  private void validate(final CourseData courseData) {
    final String operation = "when updating/creating course," + '\n';
    final Term term = courseData.getTerm();
    if (term == null || term.getSalesforceID() == null) {
      throw new SalesforceOperationException("Null 'term.salesforceID' property " + operation + courseData);
    }
    for (final Participation participation : courseData.getParticipationSet()) {
      if (participation.getInstructor().getSalesforceID() == null) {
        throw new SalesforceOperationException("Null 'instructor.salesforceID' property " + operation + courseData);
      }
    }
    if (courseData.getRoom().getSalesforceID() == null) {
      throw new SalesforceOperationException("Null 'room.salesforceID' property " + operation + courseData);
    }
    if (courseData.getCanonicalCourse().getCcn() == null) {
      throw new SalesforceOperationException("Null 'canonicalCourse.ccn' property " + operation + courseData);
    }
  }

  /**
   * @param courseDataSet
   *          list containing non-null {@link org.opencastproject.participation.model.CourseData#getTerm()}
   * @return same objects in a new structure
   */
  private Map<Integer, Map<Semester, List<CourseData>>> organize(final Set<CourseData> courseDataSet) {
    final Map<Integer, Map<Semester, List<CourseData>>> organized = new HashMap<Integer, Map<Semester, List<CourseData>>>();
    for (final CourseData courseData : courseDataSet) {
      final Integer year = courseData.getTerm().getTermYear();
      if (!organized.containsKey(year)) {
        final Map<Semester, List<CourseData>> innerMap = new HashMap<Semester, List<CourseData>>();
        organized.put(year, innerMap);
      }
      final Map<Semester, List<CourseData>> semesterMap = organized.get(year);
      final Semester semester = courseData.getTerm().getSemester();
      if (!semesterMap.containsKey(semester)) {
        semesterMap.put(semester, new LinkedList<CourseData>());
      }
      semesterMap.get(semester).add(courseData);
    }
    return organized;
  }

  /**
   * @param i1
   *          Null not allowed. Must have valid CalNetUID.
   * @param i2
   *          Null not allowed. Must have valid CalNetUID.
   * @return true if one or more properties do not match. Case variations are ignored.
   */
  private boolean updateSalesforceRecord(final Instructor i1, final Instructor i2) {
    return i1.getCalNetUID().equals(i2.getCalNetUID())
            && (!StringUtils.equalsIgnoreCase(i1.getFirstName(), i2.getFirstName())
                    || !StringUtils.equalsIgnoreCase(i1.getLastName(), i2.getLastName())
                    || !StringUtils.equalsIgnoreCase(i1.getEmail(), i2.getEmail()) || !StringUtils.equalsIgnoreCase(
                    i1.getDepartment(), i2.getDepartment()));
  }

  private Map<Instructor, SObject> getInstructorSObjectMap() {
    final SObject[] records = getRecords(SalesforceQueryType.selectAllInstructors, null);
    final Map<Instructor, SObject> map = new HashMap<Instructor, SObject>();
    for (final SObject record : records) {
      final Instructor instructor = salesforceObjectTransformer.getInstructor(record);
      if (instructor.getCalNetUID() == null) {
        logger.warn("Instructor " + instructor.getEmail() + " has NULL calNetUID");
      } else {
        map.put(instructor, record);
      }
    }
    return map;
  }

  private Map<CourseOffering, SObject> getCourseOfferingMapBySemesterYear(final Semester semester, final Integer year) {
    final Map<String, String> args = new HashMap<String, String>();
    args.put(Key.semesterTerm.name(), semester.getSalesforceValue());
    args.put(Key.semesterYear.name(), year.toString());
    final SObject[] sObjects = getRecords(SalesforceQueryType.selectCourseOfferingsBySemesterYear, args);
    return salesforceObjectTransformer.extractAssociatedCourseOfferings(sObjects, semester, year);
  }

  private String getSalesforceValue(final RepresentsSalesforceField s) {
    return (s == null) ? null : s.getSalesforceValue();
  }

  private void update(final SalesforceQueryType queryType, final Map<String, ?> queryArgs,
          final Map<HasSalesforceFieldName, ?> updates) throws NotFoundException {
    final SObject record = getRecord(queryType, queryArgs);
    for (final HasSalesforceFieldName field : updates.keySet()) {
      SalesforceUtils.setField(record, field, updates.get(field));
    }
    update(record);
  }

  private SaveResult[] update(final SObject sObject) {
    if (sObject.getId() == null) {
      throw new SalesforceOperationException("Null 'id' property for sObject: " + sObject);
    }
    return salesforceConnectorService.update(sObject);
  }

  private SaveResult create(final SObject sObject) {
    return create(Collections.list(sObject))[0];
  }

  private SaveResult[] create(final List<SObject> sObjectList) {
    try {
      return salesforceConnectorService.create(sObjectList);
    } catch (final ConnectionException e) {
      throw new SalesforceConnectionException("Failed create where sObject list size = " + sObjectList.size(), e);
    }
  }

  /**
   * @param queryType
   *          never null
   * @param key
   *          never null
   * @param value
   *          never null
   * @param <T>
   *          type of value parameter
   * @return null when not found
   */
  private <T> SObject getRecord(final SalesforceQueryType queryType, final String key, final T value) {
    final Map<String, T> queryArgs = new HashMap<String, T>(1);
    queryArgs.put(key, value);
    return getRecord(queryType, queryArgs);
  }

  /**
   * @param queryType
   *          never null
   * @param queryArgs
   *          will be injected in SOQL
   * @return null when not found
   */
  private SObject getRecord(final SalesforceQueryType queryType, final Map<String, ?> queryArgs) {
    final SObject[] records = getRecords(queryType, queryArgs);
    return (records.length >= 1) ? records[0] : null;
  }

  private SObject[] getRecords(final SalesforceQueryType queryType, final Map<String, ?> queryArgs) {
    final QueryResult queryResult = salesforceConnectorService.query(queryType, queryArgs);
    return queryResult.getRecords();
  }

  public void setSalesforceObjectTransformer(final SalesforceObjectTransformer salesforceObjectTransformer) {
    this.salesforceObjectTransformer = salesforceObjectTransformer;
  }

  public void setSalesforceConnectorService(final SalesforceConnectorService salesforceConnectorService) {
    this.salesforceConnectorService = salesforceConnectorService;
  }

  public void setNotifier(final Notifier notifier) {
    this.notifier = notifier;
  }

  // CCNs to never update - 0 is in the list because we don't want an empty list
  private static final List<Integer> NEVER_UPDATE_COURSES_IN_SALESFORCE = Collections.list(0);

  Environment getEnvironment() {
    return EnvironmentUtil.getEnvironment();
  }

  private Set<Environment> parsePropertiesToEnvironments(Properties props, String key) {
    final Set<Environment> envs = new HashSet<Environment>();

    // Environments for the key in the properties file are a comma delimited
    String fileBasedString = (StringUtils.isBlank(props.getProperty(key))) ? "" : props.getProperty(key);
    StringTokenizer tokens = new StringTokenizer(fileBasedString, ",");
    while (tokens.hasMoreTokens()) {
      envs.add(Environment.valueOf(tokens.nextToken()));
    }

    return envs;
  }

  // Properties keys
  // Package visibility
  static final String COURSE_MANAGEMENT_SERVICE_IMPL_PREFIX = "org.opencastproject.participation.impl.CourseManagementServiceImpl.";
  static final String FILE_BASED_ENVIRONMENTS = COURSE_MANAGEMENT_SERVICE_IMPL_PREFIX
          + "fileBasedCourseDataMoverEnvironments";
  static final String VIEW_BASED_ENVIRONMENTS = COURSE_MANAGEMENT_SERVICE_IMPL_PREFIX
          + "vBasedCourseDataMoverEnvironments";

  private enum Key {
    courseOfferingId, calNetUID, instructorFieldName, semesterTerm, semesterYear, adminScheduleOverride, ;
  }

  private Logger logger = LoggerFactory.getLogger(CourseManagementServiceImpl.class);

  // Next property is not 'final' because unit test will override with mock class.
  // TODO: Spring IOC to set this property
  private SalesforceObjectTransformer salesforceObjectTransformer = new SalesforceObjectTransformerImpl();
  private SalesforceConnectorService salesforceConnectorService;
  private Notifier notifier;
  private final Timer databaseLookupTimer = new Timer(true);
  private final Timer filesystemLookupTimer = new Timer(true);
}
