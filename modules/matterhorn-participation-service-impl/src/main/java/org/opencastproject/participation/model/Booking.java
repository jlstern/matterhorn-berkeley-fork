/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */

package org.opencastproject.participation.model;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * <code>Booking</code> provides an abstraction for a room that has been
 * booked, or of more interest over-booked. This last condition is a result
 * of cross-listing courses in the course catalog. A room is over-booked when 
 * two or more courses are scheduled for that room on the same days of the week 
 * starting at the same time.
 * 
 * @author Fernando Alvarez
 * 
 */
public class Booking {
  private final Term term;
  private final Room room;
  private final List<DayOfWeek> bookedDays;
  private final String bookedFrom;
  private final SortedSet<CourseData> crossListedCourses = new TreeSet<CourseData>(new CrossListedCourseDataComparator());
  
  private Booking(CourseData course) {
    this.term = course.getTerm();
    this.room = course.getRoom();
    this.bookedDays = course.getMeetingDays();
    this.bookedFrom = course.getStartTime();
    this.crossListedCourses.add(course);
  }
  
  /**
   * Implements an algorithm to reconcile the over-booking (i.e. cross-listed courses) of a room.
   * 
   * @param courses potentially cross-listed courses for the term
   * @return set of courses without any cross-listed courses 
   */
  public static Set<CourseData> reconcileCrossListedCourses(final Set<CourseData> courses) {
    final Set<CourseData> noCrossListedCourses = new HashSet<CourseData>();
    for (Booking bookedRoom : bookCourses(courses)) {
      noCrossListedCourses.add(bookedRoom.getCourseData());
    }
    return noCrossListedCourses;
  }
  
  private static Collection<Booking> bookCourses(final Set<CourseData> courses) {
     final Map<Booking, Booking> bookedRooms = new HashMap<Booking, Booking>();
     for (CourseData course : courses) {
       final Booking aBooking = new Booking(course);
       if (bookedRooms.containsKey(aBooking)) {
         bookedRooms.get(aBooking).getcrossListedCourses().add(course);
       } else {
         bookedRooms.put(aBooking, aBooking);
       }
     }
     return bookedRooms.values();
  }
  
  private CourseData getCourseData() {
    final Set<Participation> participationList = new HashSet<Participation>();
    for (CourseData c : getcrossListedCourses()) {
      participationList.addAll(c.getParticipationSet());
    }
    CourseData course = getcrossListedCourses().first();
    course.getParticipationSet().clear();
    course.getParticipationSet().addAll(participationList);
    return course;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((bookedDays == null) ? 0 : bookedDays.hashCode());
    result = prime * result + ((bookedFrom == null) ? 0 : bookedFrom.hashCode());
    result = prime * result + ((room == null) ? 0 : room.hashCode());
    result = prime * result + ((term == null) ? 0 : term.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    Booking other = (Booking) obj;
    if (bookedDays == null) {
      if (other.bookedDays != null) {
        return false;
      }
    } else if (!bookedDays.equals(other.bookedDays)) {
      return false;
    }
    if (bookedFrom == null) {
      if (other.bookedFrom != null) {
        return false;
      }
    } else if (!bookedFrom.equals(other.bookedFrom)) {
      return false;
    }
    if (room == null) {
      if (other.room != null) {
        return false;
      }
    } else if (!room.equals(other.room)) {
      return false;
    }
    if (term == null) {
      if (other.term != null) {
        return false;
      }
    } else if (!term.equals(other.term)) {
      return false;
    }
    return true;
  }
  
  private SortedSet<CourseData> getcrossListedCourses() {
    return crossListedCourses;
  }
  
  // Used to order cross-listed courses within a Booking
  private static class CrossListedCourseDataComparator implements Comparator<CourseData> {
    @Override
    public int compare(CourseData arg0, CourseData arg1) {
      return (arg0.getCanonicalCourse().getTitle().compareTo(arg1.getCanonicalCourse().getTitle()));
    }
    
  }

}
