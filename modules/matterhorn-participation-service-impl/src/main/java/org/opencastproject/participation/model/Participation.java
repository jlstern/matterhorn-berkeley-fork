/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.model;

import org.opencastproject.salesforce.SalesforceFieldInstructor;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Describes instructor preferences within the context of a course.
 * @author John King
 * @author John Crossman
 */
public class Participation {

  private Instructor instructor;
  private SalesforceFieldInstructor salesforceField;
  private boolean approved;

  public Participation() {
  }

  public Participation(final SalesforceFieldInstructor salesforceField, final Instructor instructor, final boolean approved) {
    this.salesforceField = salesforceField;
    this.instructor = instructor;
    this.approved = approved;
  }

  public Instructor getInstructor() {
    return instructor;
  }

  public void setInstructor(final Instructor instructor) {
    this.instructor = instructor;
  }

  public SalesforceFieldInstructor getSalesforceField() {
    return salesforceField;
  }

  public void setSalesforceField(final SalesforceFieldInstructor salesforceField) {
    this.salesforceField = salesforceField;
  }

  public boolean isApproved() {
    return approved;
  }

  public void setApproved(final boolean approved) {
    this.approved = approved;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }

  @Override
  public boolean equals(final Object o) {
    return (o instanceof Participation) && instructor.equals(((Participation) o).getInstructor());
  }

  @Override
  public int hashCode() {
    return instructor.hashCode();
  }

}
