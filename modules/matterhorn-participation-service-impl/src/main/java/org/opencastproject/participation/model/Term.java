/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.model;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.opencastproject.salesforce.HasSalesforceId;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * @author John Crossman
 */
public class Term implements HasSalesforceId {

  private String salesforceID;
  private Semester semester;
  private Integer termYear;
  private String semesterStartDate;
  private String semesterEndDate;

  public Term() {
  }

  public Term(final Semester semester, final Integer termYear, final String semesterStartDate,
              final String semesterEndDate) {
    this.semester = semester;
    this.termYear = termYear;
    this.semesterStartDate = semesterStartDate;
    this.semesterEndDate = semesterEndDate;
  }

  public String getSalesforceID() {
    return salesforceID;
  }

  public void setSalesforceID(final String salesforceID) {
    this.salesforceID = salesforceID;
  }

  public Semester getSemester() {
    return semester;
  }

  public void setSemester(final Semester semester) {
    this.semester = semester;
  }

  public Integer getTermYear() {
    return termYear;
  }

  public void setTermYear(final Integer termYear) {
    this.termYear = termYear;
  }

  public String getSemesterStartDate() {
    return semesterStartDate;
  }

  public void setSemesterStartDate(final String semesterStartDate) {
    this.semesterStartDate = semesterStartDate;
  }

  public String getSemesterEndDate() {
    return semesterEndDate;
  }

  public void setSemesterEndDate(final String semesterEndDate) {
    this.semesterEndDate = semesterEndDate;
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
        .append("semester", semester)
        .append("termYear", termYear)
        .append("semesterStartDate", semesterStartDate)
        .append("semesterEndDate", semesterEndDate).toString();
  }

  
  @Override
  public boolean equals(final Object that) {
    return (that instanceof Term) && new EqualsBuilder().append(this.getSemester(), ((Term) that).getSemester())
        .append(this.getTermYear(), ((Term) that).getTermYear()).isEquals();
  }

  @Override
  public int hashCode() {
    return new HashCodeBuilder().append(semester).append(termYear).toHashCode();
  }

}
