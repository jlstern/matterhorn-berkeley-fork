/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.mvc;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.opencastproject.util.env.EnvironmentUtil;
import org.springframework.web.bind.ServletRequestDataBinder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;

/**
 * @author John Crossman
 */
public final class MVCAssistant {

  private final Configuration configuration = new Configuration();

  public MVCAssistant() throws IOException {
    final File templateLoading = EnvironmentUtil.getFileUnderFelixHome("shared-resources/freemarker");
    if (!templateLoading.exists()) {
      throw new IOException("Directory for template-loading does not exist: " + templateLoading);
    }
    configuration.setDirectoryForTemplateLoading(templateLoading);
  }

  public void bind(final HttpServletRequest request, final Object obj) {
    final ServletRequestDataBinder binder = new ServletRequestDataBinder(obj);
    binder.bind(request);
  }

  public void renderTemplate(final ModelAndView mav, final HttpServletResponse response) throws IOException {
    try {
      final Template template = configuration.getTemplate(mav.getViewName() + ".ftl");
      final StringWriter result = new StringWriter();
      template.process(mav.getMap(), result);
      response.getOutputStream().write(result.toString().getBytes());
    } catch (final TemplateException e) {
      throw new IOException(e);
    }
  }

}
