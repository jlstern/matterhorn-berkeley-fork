/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.mvc;

import org.apache.commons.lang.StringUtils;
import org.opencastproject.participation.api.CourseManagementService;
import org.opencastproject.participation.model.CapturePreferences;
import org.opencastproject.participation.model.CourseOffering;
import org.opencastproject.participation.model.FinishPage;
import org.opencastproject.participation.model.RecordingAvailability;
import org.opencastproject.participation.model.RecordingType;
import org.opencastproject.participation.model.SignUp;
import org.opencastproject.security.api.SecurityService;
import org.opencastproject.security.api.User;
import org.opencastproject.util.NumberUtils;
import org.opencastproject.util.date.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Conventions;
import org.springframework.http.HttpMethod;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author John Crossman
 */
public class SignUpFormController extends HttpServlet {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  private CourseManagementService courseManagementService;
  private SecurityService securityService;
  private MVCAssistant mvcAssistant = new MVCAssistant();
  private final SignUpFormValidator validator = new SignUpFormValidator();
  private final String finishViewName;
  private final String formViewName;

  public SignUpFormController() throws IOException {
    formViewName = "index";
    finishViewName = "signUpFinish";
  }

  @Override
  protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
    doProcess(request, response, HttpMethod.GET);
  }

  @Override
  protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
    doProcess(request, response, HttpMethod.POST);
  }

  private void doProcess(final HttpServletRequest request, final HttpServletResponse response, final HttpMethod httpMethod) throws IOException {
    final User user = securityService.getUser();
    final String courseOfferingId = StringUtils.trimToNull(request.getParameter("id"));
    final CourseOffering courseOffering = (courseOfferingId == null) ? null : courseManagementService.getCourseOffering(courseOfferingId);
    final ModelAndView mav;
    if (user == null) {
      mav = getFinishPage(Message.pleaseLogIn, Message.contactUs);
    } else if (courseOfferingId == null) {
      mav = getFinishPage(Message.missingCourseId, Message.contactUs);
    } else if (courseOffering == null) {
      mav = getFinishPage(Message.noSuchCourse, Message.contactUs);
    } else if (!validator.isAuthorized(user, courseOffering.getParticipationSet())) {
      mav = getFinishPage(Message.youAreNotCourseInstructor, Message.contactUs);
    } else {
      switch (httpMethod) {
        case GET:
          setDefaultsAsNeeded(courseOffering);
          mav = new ModelAndView(formViewName);
          showForm(courseOffering, mav);
          break;
        case POST:
          final SignUp signUp = new SignUp();
          mvcAssistant.bind(request, signUp);
          final Errors errors = new BeanPropertyBindingResult(signUp, Conventions.getVariableName(signUp));
          validator.validate(signUp, user, errors);
          if (errors.hasErrors()) {
            mav = new ModelAndView(formViewName);
            showForm(courseOffering, mav);
            mav.put(signUp);
          } else {
            mav = getFinishPage(Message.thankYou, null);
            logger.warn("push stuff to Salesforce and send to Thank You page");
          }
          break;
        default:
          throw new UnsupportedOperationException("HTTP method not supported: " + httpMethod);
      }
    }
    mav.put(user);
    mav.put(courseOffering);
    mav.put(new ViewProperties(mav.getViewName()));
    mvcAssistant.renderTemplate(mav, response);
  }

  private ModelAndView getFinishPage(final Message pageTitle, final Message pageBody) throws IOException {
    final FinishPage finishPage = new FinishPage(pageTitle, pageBody);
    final ModelAndView mav = new ModelAndView(finishViewName);
    mav.put(finishPage);
    return mav;
  }

  private void showForm(final CourseOffering courseOffering, final ModelAndView mav) throws IOException {
    mav.put(courseOffering);
    mav.put(Key.startTime, DateUtil.getTimeOfDay(courseOffering.getStartTime()));
    mav.put(Key.endTime, DateUtil.getTimeOfDay(courseOffering.getEndTime()));
    mav.put(Key.publishByDayOptions, NumberUtils.range(1, 10));
    mav.put(RecordingType.values());
    mav.put(RecordingAvailability.values());
  }

  private void setDefaultsAsNeeded(final CourseOffering c) {
    c.setCourseOfferingId(c.getCourseOfferingId());
    final CapturePreferences cp = c.getCapturePreferences();
    c.setCapturePreferences(cp == null ? new CapturePreferences() : cp);
    final RecordingAvailability r = c.getCapturePreferences().getRecordingAvailability();
    c.getCapturePreferences().setRecordingAvailability(r == null ? RecordingAvailability.publicCreativeCommons : r);
    c.setParticipationSet(c.getParticipationSet());
  }

  public void setCourseManagementService(final CourseManagementService courseManagementService) {
    this.courseManagementService = courseManagementService;
  }

  public void setSecurityService(final SecurityService securityService) {
    this.securityService = securityService;
  }

}
