/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.mvc;

import org.apache.commons.lang.StringUtils;
import org.opencastproject.participation.model.Participation;
import org.opencastproject.participation.model.SignUp;
import org.opencastproject.participation.model.YesOrNo;
import org.opencastproject.security.api.DefaultOrganization;
import org.opencastproject.security.api.User;
import org.springframework.validation.Errors;

import java.util.Set;

/**
 * @author John Crossman
 */
public class SignUpFormValidator {

  public void validate(final SignUp signUp, final User user, final Errors errors) {
    if (signUp.getCourseOfferingId() == null) {
      errors.reject("error.signUp.courseOfferingIdMissing");
    }
    if (user == null) {
      errors.reject("error.signUp.logInRequired");
    } else if (!isAdminUser(user) && !signUp.isAgreeToTerms()) {
      errors.reject("error.signUp.agreeToTerms");
    }
    if (signUp.getPublishDelay() == null) {
      errors.reject("error.signUp.publishDelayUnspecified");
    } else if (YesOrNo.yes.equals(signUp.getPublishDelay()) && signUp.getPublishDelayDays() == null) {
      errors.reject("error.signUp.publishDelayDaysUnspecified");
    }
    if (signUp.getRecordingAvailability() == null) {
      errors.reject("error.signUp.recordingAvailabilityUnspecified");
    }
    if (signUp.getRecordingType() == null) {
      errors.reject("error.signUp.recordingTypeUnspecified");
    }
  }

  boolean isAuthorized(final User user, final Set<Participation> participationSet) {
    boolean authorized = false;
    if (isAdminUser(user)) {
      authorized = true;
    } else {
      final String userName = StringUtils.trimToNull(user.getUserName());
      if (participationSet != null && userName != null) {
        for (final Participation participation : participationSet) {
          if (userName.equals(participation.getInstructor().getCalNetUID())) {
            authorized = true;
            break;
          }
        }
      }
    }
    return authorized;
  }

  private boolean isAdminUser(final User user) {
    return  user.hasRole(DefaultOrganization.DEFAULT_ORGANIZATION_ADMIN);
  }

}
