/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.salesforce;

/**
 * @author John Crossman
 */
public enum SalesforceFieldInstructor {

  Instructor_1(1), Instructor_2(2), Instructor_3(3), Instructor_4(4), Instructor_5(5), Instructor_6(6),;

  private final int identifier;

  SalesforceFieldInstructor(final int positionWithinSalesforce) {
    this.identifier = positionWithinSalesforce;
  }

  public int getIdentifier() {
    return identifier;
  }

}
