/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl.persistence;

import static junit.framework.Assert.assertEquals;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.replay;
import static org.junit.Assert.assertNull;

import org.opencastproject.participation.impl.MVUtils.DateCol;
import org.opencastproject.participation.impl.MVUtils.IntCol;
import org.opencastproject.participation.impl.MVUtils.StrCol;
import org.opencastproject.participation.model.CanonicalCourse;
import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.DayOfWeek;
import org.opencastproject.participation.model.Room;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.util.date.TimeOfDay;
import org.opencastproject.salesforce.SalesforceUtils;

import org.junit.Assert;
import org.junit.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * @author John Crossman
 */
public class ResultConverterForCourseTest extends AbstractResultConverterTest {

  final String expectedCatalogId = "115 P";
  final String expectedDeptDescription = "INDUSTRIAL ENGIN AND OPER RESEARCH";
  final String expectedSectionNum = "001";

  @Test
  public void testRoomNumberParsing() throws SQLException, ParseException {
    final ResultSet r = createMockResultSet("Some Building", "  12-a ", " M W F");
    final CourseData c = new ResultConverterForCourse().convert(r);
    assertEquals("12-a", c.getRoom().getRoomNumber());
  }

  @Test
  public void testConvert() throws SQLException, ParseException {
    final ResultSet r = createMockResultSet("Some Building", "0456", " M W F");
    //
    final CourseData c = new ResultConverterForCourse().convert(r);
    final CanonicalCourse cc = c.getCanonicalCourse();
    assertEquals(123, cc.getCcn().intValue());
    assertEquals(expectedDeptDescription, cc.getDepartment());
    assertEquals(expectedDeptDescription + " " + expectedCatalogId + ", " + expectedSectionNum, cc.getTitle());
    assertEquals("0830", c.getStartTime());
    assertEquals("1100", c.getEndTime());
    assertEquals(Semester.Spring, c.getTerm().getSemester());
    assertEquals(2013, c.getTerm().getTermYear().intValue());
    final Room room = c.getRoom();
    assertEquals("Some Building", room.getBuilding());
    assertEquals("456", c.getRoom().getRoomNumber());
    final List<DayOfWeek> dayOfWeekList = new LinkedList<DayOfWeek>();
    dayOfWeekList.add(DayOfWeek.Monday);
    dayOfWeekList.add(DayOfWeek.Wednesday);
    dayOfWeekList.add(DayOfWeek.Friday);
    assertEquals(dayOfWeekList, c.getMeetingDays());
    assertEquals(expectedSectionNum, c.getSection());
    assertEquals(25, c.getStudentCount().intValue());
  }

  @Test
  public void testConvertWheelerAuditorium() throws SQLException, ParseException {
    final ResultSet r = createMockResultSet(" wheeler  auditorium   ", null, " M W F");
    //
    final CourseData c = new ResultConverterForCourse().convert(r);
    assertEquals("Wheeler", c.getRoom().getBuilding());
    assertEquals("150", c.getRoom().getRoomNumber());
  }

  @Test
  public void testUnscheduledCourse() throws SQLException, ParseException {
    final ResultSet r = createMockResultSet("  ", null, "UNSCHEDULED");
    //
    final CourseData c = new ResultConverterForCourse().convert(r);
    assertNull(c.getRoom().getBuilding());
    assertNull(c.getRoom().getRoomNumber());
    assertNull(c.getMeetingDays());
  }

  private java.sql.Date parse(final String dateString) throws ParseException {
    final Date date = SalesforceUtils.getDateFormatConvention().parse(dateString);
    return new java.sql.Date(date.getTime());
  }

  @Test
  public void testGetTimeOfDay() {
    {
      final TimeOfDay timeOfDay = ResultConverterForCourse.getTimeOfDay("0830", "A");
      Assert.assertEquals(8, timeOfDay.getHour());
      Assert.assertEquals(30, timeOfDay.getMinutes());
      Assert.assertEquals(TimeOfDay.DayPeriod.AM, timeOfDay.getDayPeriod());
    }
    {
      final TimeOfDay timeOfDay = ResultConverterForCourse.getTimeOfDay("1100", "A");
      Assert.assertEquals(11, timeOfDay.getHour());
      Assert.assertEquals(0, timeOfDay.getMinutes());
      Assert.assertEquals(TimeOfDay.DayPeriod.AM, timeOfDay.getDayPeriod());
    }
    {
      final TimeOfDay timeOfDay = ResultConverterForCourse.getTimeOfDay("1100", "P");
      Assert.assertEquals(11, timeOfDay.getHour());
      Assert.assertEquals(0, timeOfDay.getMinutes());
      Assert.assertEquals(TimeOfDay.DayPeriod.PM, timeOfDay.getDayPeriod());
    }
    {
      final TimeOfDay timeOfDay = ResultConverterForCourse.getTimeOfDay("0500", "P");
      Assert.assertEquals(5, timeOfDay.getHour());
      Assert.assertEquals(0, timeOfDay.getMinutes());
      Assert.assertEquals(TimeOfDay.DayPeriod.PM, timeOfDay.getDayPeriod());
    }
  }

  private ResultSet createMockResultSet(final String buildingName, final String roomNumber, final String meetingDays) throws SQLException, ParseException {
    final ResultSet r = createMock(ResultSet.class);
    expectReturn(r, IntCol.course_cntl_num, 123);
    expectReturn(r, StrCol.dept_description, expectedDeptDescription);
    expectReturn(r, StrCol.course_title);
    expectReturn(r, StrCol.catalog_id, expectedCatalogId);
    expectReturn(r, DateCol.term_start_date, parse("2013-08-19"));
    expectReturn(r, DateCol.term_end_date, parse("2013-12-13"));
    expectReturn(r, StrCol.meeting_start_time, "0830");
    expectReturn(r, StrCol.meeting_start_time_ampm_flag, "A");
    expectReturn(r, StrCol.meeting_end_time, "1100");
    expectReturn(r, StrCol.meeting_end_time_ampm_flag, "A");
    expectReturn(r, StrCol.term_name, "Spring");
    expectReturn(r, StrCol.term_yr, "2013");
    expectReturn(r, StrCol.building_name, buildingName);
    expectReturn(r, StrCol.room_number, roomNumber);
    expectReturn(r, StrCol.meeting_days, meetingDays);
    expectReturn(r, StrCol.section_num, expectedSectionNum);
    expectReturn(r, IntCol.student_count, 25);
    replay(r);
    return r;
  }

}
