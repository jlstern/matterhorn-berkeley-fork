/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.salesforce;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

/**
 * @author John Crossman
 */
public class SalesforceFieldInstructorTest {
  
  @Test
  public void testUniqueAndValidIdentifier() {
    final List<Integer> identifierList = new LinkedList<Integer>();
    final SalesforceFieldInstructor[] fields = SalesforceFieldInstructor.values();
    assertEquals(6, fields.length);
    for (final SalesforceFieldInstructor field : fields) {
      final Integer identifier = field.getIdentifier();
      assertFalse("Duplicate identifier: " + identifier, identifierList.contains(identifier));
      assertTrue(identifier >= 1);
      assertTrue(identifier <= 6);
      // Verify the naming convention
      assertTrue(field.name().endsWith("_" + identifier));
      identifierList.add(identifier);
    }
  }
}
