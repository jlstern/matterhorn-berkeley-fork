/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.schedulableservice.impl;

import org.opencastproject.job.api.EManagedService;
import org.opencastproject.schedulableservice.api.Periods;
import org.opencastproject.schedulableservice.api.SchedulableService;
import org.opencastproject.schedulableservice.api.SchedulableServiceService;
import org.opencastproject.security.api.DefaultOrganization;
import org.opencastproject.security.api.SecurityService;
import org.opencastproject.security.util.SecurityUtil;

import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.component.ComponentContext;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.Trigger;
import org.quartz.TriggerUtils;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.Properties;

/**
 * Schedules process that resumes publish delayed workflows.
 * 
 * @author Fernando Alvarez
 * 
 */
public class SchedulableServiceServiceImpl extends EManagedService implements SchedulableServiceService {
  
  protected synchronized void activate(ComponentContext cc) throws Exception {
    logger.info("Activating SchedulableServiceServiceImpl");

    // Starts Quartz Scheduler which is responsible for executing jobs when
    // their associated triggers fire, i.e. when their scheduled time arrives
    try {
      quartz = new StdSchedulerFactory().getScheduler();
      quartz.start();
    } catch (org.quartz.SchedulerException e) {
      logger.error("Error scheduling Quartz job", e.getMessage());
      throw new RuntimeException(e);
    }
  }
  
  @Override
  public void scheduleServiceEvent(SchedulableService service, String jobName, String userName, Periods period) {

    try {
      // Create the job based on the service being scheduled
      String jobGroup = service.getClass().getCanonicalName() + JOB_GROUP;
      final JobDetail job = new JobDetail(jobName, jobGroup, Runner.class);
      job.setDurability(false);
      job.setVolatility(true);
      job.getJobDataMap().put(JOB_PARAM_SERVICE, service);
      job.getJobDataMap().put(JOB_PARAM_ORGANIZATION, new DefaultOrganization());
      job.getJobDataMap().put(JOB_PARAM_USER, "ReplaceWithProperty");
      job.getJobDataMap().put(JOB_PARAM_SECURITY_SERVICE, this.securityService);
      quartz.addJob(job, true);

      // The trigger is the component that corresponds to the event
      Trigger trigger = makeTrigger(period);
      trigger.setStartTime(new Date());
      trigger.setName(jobName + TRIGGER_NAME);
      trigger.setGroup(jobName + TRIGGER_GROUP);
      trigger.setJobName(jobName);
      trigger.setJobGroup(jobGroup);
      logger.info("scheduling " + trigger.toString());
      
      if (quartz.getTriggersOfJob(jobName, jobGroup).length == 0) {
        quartz.scheduleJob(trigger);
      } else {
        quartz.rescheduleJob(trigger.getName(), trigger.getGroup(), trigger);
      }
    } catch (org.quartz.SchedulerException e) {
      logger.error("Error scheduling " + jobName, e.getMessage());
      throw new RuntimeException(e);
    }
  }

  // Shutdown the scheduler
  public void shutdown() {
    logger.info("Shutting down Quartz scheduler - no more schedulable service events will occur");
    try {
      quartz.shutdown();
    } catch (org.quartz.SchedulerException ignore) {
    }
  }

  // just to make sure Quartz is being shut down...
  @Override
  protected void finalize() throws Throwable {
    super.finalize();
    shutdown();
  }

  // Quartz helper class
  public static class Runner implements Job {

    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
      logger.debug("Looking up SchedulableService in Quartz JobExecutionContext");
      JobDataMap jobData = jobExecutionContext.getJobDetail().getJobDataMap();
      //TODO default org should be configurable
      DefaultOrganization defaultOrg = (DefaultOrganization) jobData.get(JOB_PARAM_ORGANIZATION);
      SchedulableService service = (SchedulableService) jobData.get(JOB_PARAM_SERVICE);
      String systemUserName = (String) jobData.get(JOB_PARAM_USER);
      SecurityService securityService = (SecurityService) jobData.get(JOB_PARAM_SECURITY_SERVICE);

      // This thread has no user or organization otherwise
      logger.debug("Calling Security Service with user {} from organization {} ", systemUserName, defaultOrg);
      securityService.setOrganization(defaultOrg);
      securityService.setUser(SecurityUtil.createSystemUser(systemUserName, defaultOrg));

      // The actual call to the service that was scheduled
      service.callBack();
      logger.debug("Completed calling SchedulableService");
    }
  }

  private Trigger makeTrigger(Periods schedulingPeriod) {
    switch (schedulingPeriod) {
      case MINUTE:
        logger.info("PublishDelayedService configured to run once per minute");
        return TriggerUtils.makeMinutelyTrigger();
      case HOUR:
        logger.info("PublishDelayedService configured to run once per hour");
        return TriggerUtils.makeHourlyTrigger();
      default:
        // Daily at 00:00 hours
        logger.info("PublishDelayedService defaulting to run once per day");
        return TriggerUtils.makeDailyTrigger(0, 0);
    }
  }
  
  //Inject reference to Security service
  private SecurityService securityService = null;
  
  public void setSecurityService(SecurityService securityService) {
    this.securityService = securityService;
  }
  
  @Override
  public void updatedConfiguration(final Properties properties) throws ConfigurationException {
    logger.info("Updating configuration for SchedulableServiceServiceImpl");
      // None at present, but might add PROPKEY_DEFAULT_ORGANIZATION
      }


  private static final Logger logger = LoggerFactory.getLogger(SchedulableServiceServiceImpl.class);
  private static final String PROPKEY_DEFAULT_ORGANIZATION = "org.opencastproject.schedulableservice.impl.SchedulableServiceServiceImpl.defaultOrg";
  private static final String JOB_GROUP = "-job-group";
  private static final String TRIGGER_NAME = "-trigger";
  private static final String TRIGGER_GROUP = "-trigger-group";
  private static final String JOB_PARAM_SERVICE = "service";
  private static final String JOB_PARAM_ORGANIZATION = "defaultOrg";
  private static final String JOB_PARAM_USER = "systemUserName";
  private static final String JOB_PARAM_SECURITY_SERVICE = "securityService";
  private org.quartz.Scheduler quartz;
}